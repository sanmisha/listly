
<?php echo $this->element('header'); ?>
<div id="page" class="theia-exception">
	<header class="header_in">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-12">
					<div id="logo" class="p-0">
						<a href="/" title="">
						<?php
							echo $this->Html->image(\Cake\Core\Configure::read('logo.name'), ['class'=>'']);
						?>
						</a>
					</div>
				</div>
				<div class="col-lg-9 col-12">
					<?= $this->element('menu'); ?>
				</div>
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</header>
	<!-- /header -->
	<main class="">
		<div id="results">
		   <div class="container">
			   <div class="row">
					<div class="col-lg-12 col-md-12 col-2">
						 <?= $this->Form->create(null,['url'=>['controller'=>'Listings', 'action'=>'search'],['class'=>'form-inline', 'id'=>'SearchForm']]) ?>
							<a href="#0" class="search_mob btn_search_mobile"></a>
							<div class="row  custom-search-input-2 inner">
								<div class="col-lg-4">
									<div class="typeahead__container">
										<?php echo $this->Form->control('listing',['label'=>false,'class'=>"form-control listing-type ft", 'type'=>"text", 'placeholder'=>"What are you looking for ... "]); ?>
									</div>
								</div>
								<!-- city -->
								<div class="col-lg-4">
									<?php
										$cities = $this->City->getCities();
										echo $this->Form->control('cities', ['label'=>false, 'options'=>$cities, 'class'=>'wide ft', 'empty'=>'Search by city']);
									?>
								</div>
								<!-- city -->
								<div class="col-lg-3">
									<?php
										$category = $this->Ctgy->getCategory();
										echo $this->Form->control('category', ['label'=>false, 'options'=>$category, 'class'=>'wide ft', 'empty'=>'Search by category']);
									?>
								</div>

								<div class="col-lg-1">
									<input type="submit" value="Search">
								</div>
							</div>
							<!-- /row -->
				         <?= $this->Form->end() ?>
					</div>
			   </div>

				<div class="search_mob_wp">
					<div class="custom-search-input-2">
                        <?= $this->Form->create(null,['url'=>['controller'=>'Listings', 'action'=>'search'],['class'=>'form-inline','valueSources' => 'query','id'=>'SearchForm']]) ?>
						<div class="form-group">
							<div class="typeahead__container">
								<?php echo $this->Form->input('listing',['label'=>false,'class'=>"form-control listing-type", 'type'=>"text", 'placeholder'=>"What are you looking for...", 'autocomplete'=>'off']); ?>
							</div>
						<!-- city -->

							<?php
								$cities = $this->City->getCities();
								echo $this->Form->control('cities', ['label'=>false, 'options'=>$cities, 'class'=>'form-control wide', 'empty'=>'search by city']);
							?>
							<?php
								$category = $this->Ctgy->getCategory();
								echo $this->Form->control('category', ['label'=>false, 'options'=>$category, 'class'=>'wide ft', 'empty'=>'Search by category']);
							?>
						</div>

						<!-- city -->


						<input type="submit" value="Search">
                        <?= $this->Form->end() ?>
					</div>
				</div>
				<!-- /search_mobile -->
		   </div>
		   <!-- /container -->
	   </div>

	   <!-- /results -->

		<div class="container mt-5 margin_60_35">
			<?php if(!empty($this->AuthUser->User('id'))) { ?>
				<div class="row">
					<div class="col-lg-12">
						<div id="filters_col"  class="col-lg-4">
							<?php $userID = $this->AuthUser->user('id'); ?>
							<a href="/Listings/index/<?= $userID ?>" class="fa fa-bookmark" id="<?= $userID ?>"> </a>
							<span ><?= $this->Html->link('My Favourites', ['controller'=>'Listings','action'=>'index', 1]) ?></span>
						</div>
					</div>
				</div>
			<?php } ?>
			<div class="row">
				<div class="col-lg-12">
					<?= $this->Flash->render(); ?>
					<div id="ajaxDiv">
						<?= $this->fetch('content'); ?>
					</div>
				</div>
				<!-- /col -->
			</div>
		</div>
		<!-- /container -->

	</main>

	<!--/main-->
<?php echo $this->element('footer'); ?>
