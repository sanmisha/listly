<?php echo $this->element('header'); ?>
<div id="page">
<header class="header_in">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-12">
					<div id="logo" class="p-0">
						<a href="/" title="">
						<?php
							echo $this->Html->image(\Cake\Core\Configure::read('logo.name'), ['class'=>'']);
						?>
						</a>
					</div>
				</div>
				<div class="col-lg-9 col-12">
					<?= $this->element('menu'); ?>
				</div>
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</header>
<!-- /header -->

<?php //debug($this->List->getListing()->toArray());exit; ?>

<main class="pattern">
	<section class="hero_single version_2">
		<div class="wrapper">
			<div class="container">
				<h3><?= $page->page_heading ?></h3>
				<p><?= $page->description ?></p>
				<?=$this->Form->create(null, ['url' => ['controller' => 'Listings', 'action' => 'search']])?>
					<div class="row no-gutters custom-search-input-2">
						<div class="col-lg-4">
							<div class="form-group">
								<div class="typeahead__container">
									<?php echo $this->Form->control('listing', ['label' => false, 'class' => "form-control listing-type", 'type' => "text", 'placeholder' => "What are you looking for...", 'autocomplete'=>'off']); ?>
								</div>
							</div>
						</div>
						<!-- city -->
						<div class="col-lg-3 form-group">
							<?php
								$cities = $this->City->getCities();
								echo $this->Form->control('cities', ['label' => false, 'options' => $cities, 'class' => 'wide', 'empty' => 'Search by City']);
							?>
						</div>
								<!-- city -->
						<div class="col-lg-3 form-group">
							<?php
								$category = $this->Ctgy->getCategory();
								echo $this->Form->control('category', ['label'=>false, 'options'=>$category, 'class'=>'wide ft', 'empty'=>'Search by Category']);
							?>
						</div>

						<div class="col-lg-2">
							<input type="submit" value="Search">
						</div>
					</div>
					<!-- /row -->
				<?=$this->Form->end()?>
			</div>
		</div>
	</section>

	<!-- /hero_single -->

	<div class="main_categories">
		<div class="container">
			<ul class="clearfix">
				<?php
					$categories = $this->Ctgy->getHomepageCategory();
					foreach ($categories as $category) {
				?>
				<li>
					<a href="/listings/search/<?=$category->slug ?>">
						<i class="icon-<?php echo $category->icon ?>"></i>
						<h3><?php echo $category->category ?></h3>
					</a>
				</li>
			<?php }?>
			</ul>
		</div>
		<!-- /container -->
	</div>
	<!-- /main_categories -->
	<div class="container margin_60_35">
        <?php
            echo $this->Flash->render();
            // $content = $this->fetch('content');
            /* echo eval('?>' . $content); */
        ?>
		<div class="main_title_3">
			<span></span>
			<h2>Popular Categories</h2>
			<a href="/listings">See all</a>
		</div>
		<div class="row add_bottom_30">
			<?php foreach ($this->Ctgy->getRandCategory() as $category) { //debug($category->toArray());exit; ?>
			<div class="col-lg-3 col-sm-6 col-6">
				<a href="/listings/search/<?=$category->slug ?>" class="grid_item small">
					<figure>
						<?=$this->Html->image('/files/categories/photo/' . $category->photo_dir . '/proper_' . $category->photo, ['class' => 'img-fluid'])?>
						<div class="info">
							<h3><?=$category->category?></h3>
						</div>
					</figure>
				</a>
			</div>
			<?php }?>
		</div>
	</div>

	<div class="container margin_60_35">
		<div class="main_title_3">
			<span></span>
			<h2>Popular Listings</h2>
			<a href="/listings">See all</a>
		</div>
		<div class="row add_bottom_30">
			<?php foreach ($this->List->getListing() as $listing) { //debug($listing->toArray());exit; ?>
			<div class="col-lg-3 col-sm-6 col-6">

				<a href="/<?=$listing->category->slug?>/<?=$listing->slug?>" class="grid_item small">
					<figure>
						<?=$this->Html->image('/files/listings/banner/' . $listing->banner_dir . '/proper_' . $listing->banner, ['class' => 'img-fluid'])?>
						<div class="info">
							<h3><?=$listing->listing_title?></h3>
						</div>
					</figure>
				</a>
			</div>
			<?php } ?>
		</div>

	</div>
	<!-- /container -->

	<div class="call_section">
		<div class="wrapper">
			<div class="container margin_80_55">
				<div class="main_title_2">
					<span><em></em></span>
					<?php foreach($all as $one){ 
						if($one->section == "how_it_works-1"){ ?>

					<h2>How it Works</h2>
					<p><?= $one->contents ?></p>
					<?php } } ?>
				</div>
			
				<div class="row">
					<?php $arr = ["how_it_works-4", "how_it_works-2", "how_it_works-3"];
					foreach($all as $one) { 
						if(in_array($one->section, $arr)){ ?>
					<div class="col-md-4">
						<div class="box_how">
							<i class="pe-7s-info"></i>
							<h3>View Location Info</h3>
							<?= $one->contents ?>
						</div>
					</div>	
					<?php } } ?>				
				</div>
				<p class="text-center add_top_30 wow bounceIn" data-wow-delay="0.5s"><a href="/members/register" class="btn_1 rounded">Register Now</a></p>
			</div>
			<canvas id="hero-canvas" width="1920" height="1080"></canvas>
		</div>
	</div>

	<!--/call_section-->
<!-- testimonials -->

<!-- <div class="container-fluid margin_80_55">
	<div class="main_title_2">
		<span><em></em></span>
		<h2>Testimonials</h2>
	</div>
	<div id="reccomended" class="owl-carousel owl-theme">
	<div class="item">
			<div class="strip grid">
				<div class="wrapper">
					<p><i class="ti ti-quote-left" style="font-size: 24px;color: #ed2025;"></i> Text will come here Text will come here Text will come here Text will come here Text will come here Text will come here Text will come here Text will come here Text will come here Text will come here Text will come here Text will come here </p>
					<h6><strong>Name</strong> - Location</h6>
				</div>
			</div>
		</div>
	</div>
</div> -->
<!-- /container -->

</main>
<!-- /main -->
<?php echo $this->element('footer'); ?>
