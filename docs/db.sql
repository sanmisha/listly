/*
SQLyog Community v13.1.9 (64 bit)
MySQL - 10.4.24-MariaDB : Database - sanmiuce_listly
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sanmiuce_listly` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `sanmiuce_listly`;

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `photo_dir` varchar(255) DEFAULT NULL,
  `popular` tinyint(1) DEFAULT 0,
  `display_on_homepage` tinyint(1) DEFAULT 0,
  `icon` varchar(255) DEFAULT NULL,
  `list_order` varchar(3) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

/*Data for the table `categories` */

insert  into `categories`(`id`,`category`,`slug`,`description`,`photo`,`photo_dir`,`popular`,`display_on_homepage`,`icon`,`list_order`,`modified_by`,`modified`,`created_by`,`created`) values 
(28,'Sports','sports','Sports Equipment\'s','sports banner hr 1.JPG','5bf5a744-aacb-47c7-9edf-d3d2f8e83cdf',1,1,'shop','1',NULL,'2022-11-18 03:05:50',NULL,'2022-04-02 10:33:09'),
(29,'Gardening','gardening','Gardening Tools and equipments','gardenning banner hr 1.JPG','457aeef3-d2b3-4237-9025-91c493ada8b4',1,1,'shop','3',NULL,'2022-11-18 03:05:18',NULL,'2022-04-15 10:01:39'),
(30,'Tools','tools','Tools','tools banner hr 1.JPG','cd5f261f-6f78-433a-ab2a-fa7270ef5828',1,1,'shop','2',NULL,'2022-11-18 03:05:34',NULL,'2022-04-21 17:28:16'),
(31,'Camping','camping','Rent everything you need for Camping','camping banner hr 1.JPG','4006f64f-f04c-4049-9a04-603f4b29490b',1,1,'shop','6',NULL,'2022-11-18 03:04:27',NULL,'2022-09-11 23:38:44'),
(32,'Party Rentals','party-rentals','Rent everything you need for a Party','party banner hr 1.JPG','e8a8d69d-a219-47bf-a72c-83da56de2385',1,1,'shop','4',NULL,'2022-11-18 03:05:02',NULL,'2022-09-11 23:43:13'),
(33,'Appliances','appliances','Rent all you need under Appliances','appliances banner hr 1.JPG','374922cd-c5a0-42b2-9502-b997b91b45df',1,1,'shop','5',NULL,'2022-11-18 03:04:43',NULL,'2022-09-11 23:56:09'),
(34,'Household','household','Rent everything for Household ','household banner hr 1.JPG','dad8ce4c-8bf9-43ce-8460-9a7c845da59b',1,1,'shop','7',NULL,'2022-11-18 03:07:39',NULL,'2022-09-12 00:06:52'),
(35,'Seasonal ','seasonal','Seasonal ','seasonal hr 1.JPG','75cbda61-054f-4be6-8f87-78685b348dce',1,1,'shop','',NULL,'2022-11-18 02:57:12',NULL,'2022-09-19 05:28:24'),
(36,'Travel','travel','Travel Accessories','travel bag.JPG','eb65b7ed-73b3-4286-a7fa-05443c3edd18',1,1,'basket','6',NULL,'2023-02-12 16:03:22',NULL,'2023-02-12 16:03:22');

/*Table structure for table `cities` */

DROP TABLE IF EXISTS `cities`;

CREATE TABLE `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

/*Data for the table `cities` */

insert  into `cities`(`id`,`country_id`,`state_id`,`city`) values 
(20,2,40,'Calgary'),
(21,2,40,'Edmonton'),
(26,2,40,'Lethbridge'),
(29,2,45,'Vancouver'),
(30,2,40,'Edmonton'),
(31,2,40,'Toronto'),
(32,2,40,'Okotoks');

/*Table structure for table `classifications` */

DROP TABLE IF EXISTS `classifications`;

CREATE TABLE `classifications` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `classification` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `photo_dir` varchar(255) DEFAULT NULL,
  `populer` tinyint(1) DEFAULT NULL,
  `list_order` varchar(3) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `classifications` */

insert  into `classifications`(`id`,`classification`,`slug`,`description`,`photo`,`photo_dir`,`populer`,`list_order`,`modified_by`,`modified`,`created_by`,`created`) values 
(1,'abc demo','abc-demo','hello','battle.jpg','626323dd-cb3c-4085-a176-00c3ce3b129f',NULL,'3',NULL,'2021-03-06 14:17:00',NULL,'2021-03-06 14:15:54'),
(2,'demo new new','demo-new-new','Hello','battle.jpg','b0ff88a0-4ffb-47c9-b735-13e0fdadef3e',NULL,'3',NULL,'2021-03-06 14:38:47',NULL,'2021-03-06 14:38:47');

/*Table structure for table `comments` */

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `listing_id` int(11) DEFAULT NULL,
  `rating` tinyint(2) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT 1,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

/*Data for the table `comments` */

insert  into `comments`(`id`,`listing_id`,`rating`,`comment`,`user_id`,`approved`,`modified`,`created`) values 
(1,3,1,'Amar\r\n',75,1,'2022-05-12 13:00:43','2022-05-12 13:00:43'),
(2,3,2,'two\r\n',75,1,'2022-05-12 13:00:50','2022-05-12 13:00:50'),
(3,3,5,'Good Service',75,1,'2022-05-12 13:07:22','2022-05-12 13:07:22'),
(4,25,4,'I am really impressed by the quality of services I received from Flatworld Solutions. You were right on schedule, charged reasonable prices, were professional and courteous in dealings, and delivered items well before time',75,1,'2022-05-12 17:40:38','2022-05-12 17:40:38'),
(5,25,5,' I have got a good e-commerce site for my products. My revenue has increased because of Flatworld and I will definitely use your services again.',75,1,'2022-05-12 17:40:58','2022-05-12 17:40:58'),
(6,25,5,'We thank Sanmisha Technologies for the wonderful job in helping us develop our program. Everyone was professional, excellent and hard working. Thanks to them, we were able to achieve our goal on time, and we look forward to continue working with them in the future.',1,1,'2022-05-12 17:44:54','2022-05-12 17:44:54'),
(7,25,3,'\r\n I have got a good e-commerce site for my products. My revenue has increased because of Sanmisha Technologies and I will definitely use your services again.',1,1,'2022-05-12 17:46:42','2022-05-12 17:46:42'),
(8,34,5,'Very popular shop and very good products they sell',1,1,'2022-06-14 13:08:19','2022-06-14 13:08:19'),
(9,34,5,'',1,1,'2022-06-14 13:09:17','2022-06-14 13:09:17'),
(10,34,5,'',1,1,'2022-06-14 13:31:16','2022-06-14 13:31:16'),
(11,13,5,'',129,1,'2022-06-17 16:13:28','2022-06-17 16:13:28'),
(12,13,5,'',129,1,'2022-06-22 12:07:03','2022-06-22 12:07:03'),
(13,34,5,'if you are finding something new yet traditional, then its only the one shop HARIPRIYA COLLECTION',158,1,'2022-06-22 13:49:21','2022-06-22 13:49:21'),
(14,39,5,'',1,1,'2022-06-26 12:23:05','2022-06-26 12:23:05'),
(15,56,4,'Very Good',3,1,'2022-06-30 17:58:56','2022-06-30 17:58:56'),
(16,56,2,'Very satisfied by the product',4,1,'2022-07-06 10:31:41','2022-07-06 10:31:41'),
(17,56,1,'Test Review',4,1,'2022-07-11 17:45:22','2022-07-11 17:45:22'),
(18,56,1,'Test Review 2',4,1,'2022-07-11 17:45:33','2022-07-11 17:45:33'),
(19,56,1,'Test Review 3',4,1,'2022-07-11 17:45:45','2022-07-11 17:45:45'),
(20,56,1,'Test Review 5',4,1,'2022-07-11 17:46:48','2022-07-11 17:46:48'),
(21,56,1,'Test Review 6',4,1,'2022-07-11 17:47:07','2022-07-11 17:47:07'),
(22,56,1,'Test Review 7',4,1,'2022-07-11 17:47:21','2022-07-11 17:47:21'),
(23,57,1,'test 1',4,1,'2022-07-11 17:48:11','2022-07-11 17:48:11'),
(24,57,1,'test 2',4,1,'2022-07-11 17:48:20','2022-07-11 17:48:20'),
(25,56,5,'very good',10,1,'2022-07-23 18:10:02','2022-07-23 18:10:02'),
(26,56,3,'Awesome work',4,1,'2022-08-03 10:39:07','2022-08-03 10:39:07'),
(27,56,3,'Awesome work',4,1,'2022-08-03 10:39:56','2022-08-03 10:39:56'),
(28,56,4,'Test Review',4,1,'2022-08-25 10:19:48','2022-08-25 10:19:48'),
(29,57,3,'test review',4,1,'2022-08-25 10:22:08','2022-08-25 10:22:08'),
(30,61,4,'timely service with great variety of furniture',4,1,'2022-09-12 10:21:48','2022-09-12 10:21:48'),
(31,65,4,'Good quality and gas efficient. Cuts well',17,1,'2022-09-20 13:14:36','2022-09-20 13:14:36'),
(32,64,5,'Good quality bike',4,1,'2022-10-13 22:52:29','2022-10-13 22:52:29'),
(33,63,3,'Not so satisfied ',4,1,'2022-10-13 22:53:23','2022-10-13 22:53:23'),
(34,65,4,'Good quality and gas efficient. Cuts well',4,1,'2022-10-13 22:55:08','2022-10-13 22:55:08'),
(35,62,4,'Test review',4,1,'2022-10-14 11:51:34','2022-10-14 11:51:34'),
(36,62,5,'Test review 2 ',4,1,'2022-11-15 23:56:58','2022-11-15 23:56:58'),
(37,62,3,'Test review 3',4,1,'2022-11-15 23:57:10','2022-11-15 23:57:10'),
(38,62,5,'Test review 4',4,1,'2022-11-15 23:57:26','2022-11-15 23:57:26'),
(39,61,5,'Good service ',4,1,'2022-12-29 07:44:33','2022-12-29 07:44:33'),
(40,67,3,'',4,1,'2023-01-01 14:32:01','2023-01-01 14:32:01'),
(41,67,5,'Test msg 2',4,1,'2023-01-01 14:42:37','2023-01-01 14:42:37'),
(42,71,4,'Very good condition at good price',5,1,'2023-02-05 07:04:30','2023-02-05 07:04:30'),
(43,71,4,'Very good condition at good price',5,1,'2023-02-05 07:04:42','2023-02-05 07:04:42'),
(44,71,3,'Good for the price. ',17,1,'2023-02-05 07:17:06','2023-02-05 07:17:06'),
(45,62,5,'Very good product',4,1,'2023-02-26 19:42:58','2023-02-26 19:42:58'),
(46,69,1,'Good',4,1,'2023-03-04 14:42:11','2023-03-04 14:42:11'),
(47,62,1,'Test ',4,1,'2023-03-04 14:45:38','2023-03-04 14:45:38'),
(48,63,3,'',36,1,'2023-03-09 02:19:20','2023-03-09 02:19:20'),
(49,63,2,'test',36,1,'2023-03-09 02:19:41','2023-03-09 02:19:41'),
(50,63,1,'test',36,1,'2023-03-09 02:20:29','2023-03-09 02:20:29'),
(51,63,1,'test',36,1,'2023-03-09 02:23:00','2023-03-09 02:23:00'),
(52,72,5,'tets',4,1,'2023-03-09 02:50:10','2023-03-09 02:50:10'),
(53,67,3,'test 1',35,1,'2023-03-09 03:34:12','2023-03-09 03:34:12'),
(54,67,3,'test 2',35,1,'2023-03-09 03:34:27','2023-03-09 03:34:27'),
(55,67,3,'test 3\r\n',35,1,'2023-03-09 03:34:38','2023-03-09 03:34:38'),
(56,67,3,'test 4\r\n',35,1,'2023-03-09 03:34:56','2023-03-09 03:34:56'),
(57,67,1,'\r\n',35,1,'2023-03-09 03:35:20','2023-03-09 03:35:20'),
(58,67,1,'',35,1,'2023-03-09 03:35:27','2023-03-09 03:35:27'),
(59,67,1,'',35,1,'2023-03-09 03:35:33','2023-03-09 03:35:33'),
(60,67,1,'',35,1,'2023-03-09 03:35:40','2023-03-09 03:35:40'),
(61,67,1,'',35,1,'2023-03-09 03:35:52','2023-03-09 03:35:52'),
(62,67,1,'',35,1,'2023-03-09 03:36:00','2023-03-09 03:36:00'),
(63,67,1,'',35,1,'2023-03-09 03:36:06','2023-03-09 03:36:06'),
(64,67,1,'',35,1,'2023-03-09 03:36:12','2023-03-09 03:36:12'),
(65,67,1,'',35,1,'2023-03-09 03:36:19','2023-03-09 03:36:19'),
(66,65,5,'lo',26,1,'2023-03-10 08:10:09','2023-03-10 08:10:09'),
(67,72,2,'',30,1,'2023-03-11 05:03:36','2023-03-11 05:03:36');

/*Table structure for table `countries` */

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country` varchar(50) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `countries` */

insert  into `countries`(`id`,`country`,`modified_by`,`modified`,`created_by`,`created`) values 
(2,'Canada',1,'2023-02-03 08:12:15',1,'2022-04-02 10:20:11'),
(3,'United States Of America',1,'2022-07-17 09:17:34',1,'2022-07-17 09:17:34');

/*Table structure for table `favourites` */

DROP TABLE IF EXISTS `favourites`;

CREATE TABLE `favourites` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `listing_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

/*Data for the table `favourites` */

insert  into `favourites`(`id`,`listing_id`,`user_id`,`modified_by`,`modified`,`created_by`,`created`) values 
(25,61,4,4,'2022-09-12 10:21:13',4,'2022-09-12 10:21:13'),
(27,61,16,16,'2022-10-14 06:30:21',16,'2022-10-14 06:30:21'),
(28,63,16,16,'2022-10-14 06:31:30',16,'2022-10-14 06:31:30'),
(32,62,27,27,'2023-01-06 05:39:41',27,'2023-01-06 05:39:41'),
(33,68,27,27,'2023-01-06 05:39:46',27,'2023-01-06 05:39:46'),
(34,71,17,17,'2023-02-05 07:19:34',17,'2023-02-05 07:19:34'),
(35,65,17,17,'2023-02-05 07:19:44',17,'2023-02-05 07:19:44'),
(36,67,17,17,'2023-02-05 07:19:49',17,'2023-02-05 07:19:49'),
(51,68,4,4,'2023-03-03 19:04:08',4,'2023-03-03 19:04:08'),
(54,64,4,4,'2023-03-03 19:56:37',4,'2023-03-03 19:56:37'),
(55,63,36,36,'2023-03-09 02:23:33',36,'2023-03-09 02:23:33'),
(56,73,17,17,'2023-03-09 05:46:01',17,'2023-03-09 05:46:01'),
(57,72,30,30,'2023-03-11 05:03:46',30,'2023-03-11 05:03:46'),
(58,71,4,4,'2023-03-15 04:36:18',4,'2023-03-15 04:36:18'),
(60,61,26,26,'2023-03-15 11:59:10',26,'2023-03-15 11:59:10'),
(63,62,29,29,'2023-03-17 07:54:48',29,'2023-03-17 07:54:48'),
(66,65,29,29,'2023-03-17 07:55:31',29,'2023-03-17 07:55:31'),
(67,65,1,1,'2023-03-18 06:29:45',1,'2023-03-18 06:29:45'),
(68,66,26,26,'2023-03-28 12:57:48',26,'2023-03-28 12:57:48'),
(69,73,26,26,'2023-03-31 05:23:40',26,'2023-03-31 05:23:40'),
(70,70,26,26,'2023-03-31 05:28:00',26,'2023-03-31 05:28:00');

/*Table structure for table `listings` */

DROP TABLE IF EXISTS `listings`;

CREATE TABLE `listings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `listing_title` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `address` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `description` text CHARACTER SET utf8 DEFAULT NULL,
  `google_location` varchar(500) DEFAULT NULL,
  `gps` varchar(100) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `postal_code` varchar(10) DEFAULT NULL,
  `hourly_rate` decimal(10,2) DEFAULT NULL,
  `daily_rate` decimal(10,2) DEFAULT NULL,
  `currency` varchar(20) DEFAULT NULL,
  `business_hours` text DEFAULT NULL,
  `open_24_7` tinyint(1) DEFAULT 0,
  `banner` varchar(255) DEFAULT NULL,
  `banner_dir` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_dir` varchar(255) DEFAULT NULL,
  `image_1` varchar(255) DEFAULT NULL,
  `image_1_dir` varchar(255) DEFAULT NULL,
  `image_2` varchar(255) DEFAULT NULL,
  `image_2_dir` varchar(255) DEFAULT NULL,
  `image_3` varchar(255) DEFAULT NULL,
  `image_3_dir` varchar(255) DEFAULT NULL,
  `image_4` varchar(255) DEFAULT NULL,
  `image_4_dir` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 0,
  `premium` tinyint(1) DEFAULT 0,
  `popular` tinyint(1) DEFAULT 0,
  `hide_contact` tinyint(1) DEFAULT 0,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;

/*Data for the table `listings` */

insert  into `listings`(`id`,`user_id`,`category_id`,`sub_category_id`,`listing_title`,`slug`,`address`,`description`,`google_location`,`gps`,`country_id`,`state_id`,`city_id`,`email`,`mobile`,`postal_code`,`hourly_rate`,`daily_rate`,`currency`,`business_hours`,`open_24_7`,`banner`,`banner_dir`,`image`,`image_dir`,`image_1`,`image_1_dir`,`image_2`,`image_2_dir`,`image_3`,`image_3_dir`,`image_4`,`image_4_dir`,`active`,`premium`,`popular`,`hide_contact`,`modified_by`,`modified`,`created_by`,`created`) values 
(61,4,32,75,'Wedding Supplies','wedding-supplies','6455 Macleod Trail, Calgary, AB T2H 0K8 nnnnnnnnnnnn ','<p class=\"MsoNormal\" style=\"margin-bottom:0cm;line-height:normal\"><span lang=\"FR\">Sample Description Sample Description <o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom:0cm;line-height:normal\"><span lang=\"FR\">Sample Description Sample Description</span></p><p class=\"MsoNormal\" style=\"margin-bottom:0cm;line-height:normal\"><span lang=\"FR\">Sample Description Sample Description <o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom:0cm;line-height:normal\"><br></p><p class=\"MsoNormal\" style=\"margin-bottom:0cm;line-height:normal\"><o:p></o:p></p>','50.99863,-114.07326','',2,NULL,20,'anand.bhadekar@gmail.com','1234567890','',10.00,50.00,'CAD','{\"Monday\":[\"\",\"\"],\"Tuesday\":[\"\",\"\"],\"Wednesday\":[\"\",\"\"],\"Thursday\":[\"\",\"\"],\"Friday\":[\"\",\"\"],\"Saturday\":[\"\",\"\"],\"Sunday\":[\"\",\"\"]}',0,'four-corners_1140_658_80_s.jpg','2c9a61b0-4fc0-417f-b00f-e15f851c6c38',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',1,0,1,0,NULL,'2023-03-12 18:35:49',NULL,'2022-09-12 10:18:44'),
(62,4,35,79,'Kayak - Inflatable ','kayak-inflatable','204 A 3221 Markham Place','Related FAQ\r\nHere are some more FAQ related to this topic:\r\n\r\nHow to remove white space underneath an image using CSS\r\nHow to create an unordered list without any bullets in HTML\r\nHow to disable a link using only CSS','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2510.9554122556597!2d-114.07553358424867!3d50.998496079554734!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x537170558c817a7b%3A0xa80ab79074859579!2sCF%20Chinook%20Centre!5e0!3m2!1sen!2sca!4v1663545851629!5m2!1sen!2sca\" width=\"100%\" height=\"400\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>','',2,NULL,29,'anand.bhadekar@gmail.com','4169953879','M1B0B3',5.00,30.00,'','{\"Monday\":[\"\",\"\"],\"Tuesday\":[\"\",\"\"],\"Wednesday\":[\"\",\"\"],\"Thursday\":[\"\",\"\"],\"Friday\":[\"\",\"\"],\"Saturday\":[\"\",\"\"],\"Sunday\":[\"\",\"\"]}',0,'kayak hr 1.JPG','29629e0a-098f-4545-9057-2d8be4ab4e81','kayak hr 2.JPG','36f96dc6-952e-454a-8ccf-43cb4a14b672','Kayak 3.JPG','ce20a1bf-47b6-41df-9ee0-663cf51f1192',NULL,'',NULL,'',NULL,'',1,1,1,0,NULL,'2023-03-15 12:16:08',NULL,'2022-09-19 05:37:38'),
(63,4,28,80,'E-Power iLink 180 Race','e-power-ilink-180-race','Pacific Center Mall, Vancouver BC','<p class=\"MsoNormal\" style=\"margin-bottom: 0cm; line-height: normal;\"><span lang=\"FR\">Sample Description Sample Description<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 0cm; line-height: normal;\"><span lang=\"FR\">Sample Description Sample Description</span></p><p class=\"MsoNormal\" style=\"margin-bottom: 0cm; line-height: normal;\"><span lang=\"FR\">Sample Description Sample Description</span></p><p class=\"MsoNormal\" style=\"margin-bottom: 0cm; line-height: normal;\"><span lang=\"FR\"><br></span></p><p>E-Power iLink 180 Race bike\r\nCompact, lightweight and with more power – Performance Line CX\r\n\r\nEquipped with Power Tube\r\n\r\nPurion enabled for highest efficiency and precision.</p>','<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d10043.821666243302!2d-114.07334500000002!3d50.998496!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa80ab79074859579!2sCF%20Chinook%20Centre!5e0!3m2!1sen!2sca!4v1665739367491!5m2!1sen!2sca\" width=\"100%\" height=\"400\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>','',2,45,29,'anand.bhadekar@gmail.com','4169953879','M1B0B3',25.00,150.00,'CAD','8am-5pm',0,'bike hr 1.JPG','a48e1c5b-830e-4e50-b228-796d166e422c','ebike 2.JPG','879b878c-537c-4cac-aee0-5d59d386b065',NULL,'',NULL,'',NULL,'',NULL,'',1,0,1,0,NULL,'2022-11-18 03:26:33',NULL,'2022-09-19 05:46:20'),
(64,17,28,80,'e-bike','e-bike','South Center Mall, Calgary AB','<p class=\"MsoNormal\">AQUAGLIDE MCKENZIE 125 INFLATABLE KAYAK<o:p></o:p></p><p class=\"MsoNormal\" style=\"margin-bottom:0cm;line-height:normal;background:#F2F2F2\">Length&nbsp;11’2″&nbsp;|<o:p></o:p></p><p class=\"MsoNormal\" style=\"margin-bottom:0cm;line-height:normal;background:#F2F2F2\">Width&nbsp;37″&nbsp;|<o:p></o:p></p><p class=\"MsoNormal\" style=\"margin-bottom:0cm;line-height:normal;background:#F2F2F2\">Product Weight&nbsp;31.5 LBS.\r\n(14.3 KG)&nbsp;|<o:p></o:p></p><p class=\"MsoNormal\" style=\"margin-bottom:0cm;line-height:normal;background:#F2F2F2\">Max Weight&nbsp;400 LBS. (181 KG)<o:p></o:p></p><p class=\"MsoNormal\"><b><span style=\"font-family: Poppins; background: rgb(242, 242, 242);\">Best for:</span></b><span style=\"font-family: Poppins; background: rgb(242, 242, 242);\">&nbsp;Lakes, Bays, Inland Waters, Slow Moving\r\nRivers (Class I-II)</span><o:p></o:p></p><p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p class=\"MsoNormal\">Note - Requires Halkey-Roberts Style Valve adaptor to\r\ninflate properly.<o:p></o:p></p>','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2513.440802355686!2d-114.06844598428357!3d50.952553879547885!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x537176cc82d44277%3A0x48b3bbdc69c05da9!2sSouthcentre%20Mall!5e0!3m2!1sen!2sca!4v1663548270505!5m2!1sen!2sca\" width=\"100%\" height=\"400\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>','',2,40,20,'testclient464','56565656565656565656','',30.00,150.00,'CAD','Monday 8-10\r\nTuesday 8-10\r\nWednesday 8-10\r\nThursday 8-10\r\nFriday 8-10\r\nSaturday 10-4\r\nSunday Closed',0,'e bike hr 1.JPG','6bd570a3-4744-4be2-b492-8b52ddc3d95e','e bike hr 2.JPG','033a5328-31a5-4f4c-8d9c-d442498c9b80',NULL,'',NULL,'',NULL,'',NULL,'',1,0,1,0,NULL,'2023-02-22 04:24:27',NULL,'2022-09-19 06:16:08'),
(65,17,30,81,'Lawn-Mover','lawn-mover','Oak Ridge Center Mall,  Vancouver BC','CRAFTSMAN 21-in 159cc 3-in-1 Push Lawn Mower\r\n\r\nThe 159cc overhead valve 4-cycle engine is easy starting with the auto choke system\r\nThe check don\'t change engine oil feature eliminates the need to change the oil for the life of the machine\r\nHigh rear wheels make turning and maneuvering easier\r\nThe dust-blocking grass bag helps to keep dust off the operator during the driest conditions\r\nDeck wash-out port allows you to clean the underside of the cutting deck simply with a garden hose connection\r\nThe 3 position adjustable ergo handle will fit most operators\r\nCraftsman three year limited warranty is supported by our Canadian call center and nation-wide service network','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2605.3396790782813!2d-123.12087038431235!3d49.23204877932589!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54867470a79813d9%3A0x582eecaac979ee7a!2s650%20W%2041st%20Ave%2C%20Vancouver%2C%20BC%20V5Z%202M9!5e0!3m2!1sen!2sca!4v1663679109575!5m2!1sen!2sca\" width=\"100%\" height=\"400\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>','',2,45,29,'testclient464@gmail.com','1121212121212','T2J3V1',15.00,60.00,'CAD','8am-6pm',0,'lawnmover hr 1.JPG','33a39453-13a2-4c93-b993-6bf3d5eee107',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',1,1,1,0,NULL,'2023-02-22 04:21:57',NULL,'2022-09-20 13:12:14'),
(66,19,35,79,'Leaf Blower (Electric)','leaf-blower-(electric)','539 Belmont Heath SW','','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2518.0925426209883!2d-114.06843938425348!3d50.866486479534956!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x53719e1ffda6ac47%3A0x343d50a70a50a3c!2s539%20Belmont%20Heath%20SW%2C%20Calgary%2C%20AB%20T2X%200K4!5e0!3m2!1sen!2sca!4v1668271693378!5m2!1sen!2sca\" width=\"100%\" height=\"400\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>','',2,40,20,'testclient512@gmail.com','5555555555','T2J 3V1',15.00,75.00,'CAD','Mon - Friday :8am - 4pm\r\nWeekends: 10am-4pm',0,'leafblower hr 1.JPG','03c881e6-55bb-408c-bc74-4c8eec9c867b','Leaf Blower 2.JPG','6f741475-86c7-462b-b796-ad312e22de63','LEaf Blower 3.JPG','a5b1ad06-b3b6-4c60-903d-e1f32a0870ae',NULL,'',NULL,'',NULL,'',1,0,1,0,NULL,'2023-02-22 04:21:25',NULL,'2022-11-12 16:51:44'),
(67,19,34,82,'Wet Vacuum','wet-vacuum','6020 Thetis PL Richmond BC','<h1 style=\"margin-top:0cm;margin-right:0cm;margin-bottom:3.75pt;margin-left:\r\n0cm;vertical-align:baseline\"><p class=\"MsoNormal\"><span style=\"font-size:10.0pt;line-height:107%\">Multi-Surface\r\nKit which includes Extra Brush Rolls, 32oz Formula</span></p><p class=\"MsoNormal\"><span style=\"font-size: 10pt;\">Is safe and\r\neffective on tile, sealed wood floors, laminate, linoleum, vinyl, rubber floor\r\nmats, pressed wood floors, area rugs</span></p><p class=\"MsoNormal\"><o:p></o:p></p></h1><h1 style=\"margin-top:0cm;margin-right:0cm;margin-bottom:3.75pt;margin-left:\r\n0cm;vertical-align:baseline\"><span style=\"font-size: 10pt; font-family: Calibri, sans-serif;\"><o:p></o:p></span></h1>','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d425.2668735017265!2d-123.15612412025035!3d49.16932475732077!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54860ad001dbb279%3A0x17d6b8653d5e7589!2s6020%20Thetis%20Pl%2C%20Richmond%2C%20BC%20V7C%202N3!5e0!3m2!1sen!2sca!4v1668272999414!5m2!1sen!2sca\" width=\"100%\" height=\"400\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>','',2,45,29,'testclient512@gmail.com','2222222222','VC72N3',25.00,100.00,'CAD','Monday 8-10\r\nTuesday 8-10\r\nWednesday 8-10\r\nThursday 8-10\r\nFriday 8-10\r\nSaturday 8-10',0,'wet vaccum hr 1.JPG','d77caf18-4b4f-4275-b29b-53d32dbb60f6','Wet Vaccum 2.JPG','55ab01a7-2930-4d37-bf94-4a21ff0ac128','Wet Vaccum 3.JPG','501a1ffe-0ee0-4acd-98a1-bb5e6e453f39',NULL,'',NULL,'',NULL,'',1,0,1,0,NULL,'2023-02-22 04:21:12',NULL,'2022-11-12 17:25:08'),
(68,19,28,83,'Golf Kit','golf-kit','4 Sharp Crescent Ajax ON','<p style=\"margin: 0cm 22.5pt 6pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><br></p><ul><li style=\"position: relative; padding: 3px 0px 3px 15px; list-style: none;\">460cc 10.5 degree with graphite shaft</li><li style=\"position: relative; padding: 3px 0px 3px 15px; list-style: none;\">18 degree #3 fairway metal with graphite shaft</li><li style=\"position: relative; padding: 3px 0px 3px 15px; list-style: none;\">21 degree and 24 degree stainless steel hybrid clubs</li><li style=\"position: relative; padding: 3px 0px 3px 15px; list-style: none;\">5-PW stainless steel irons with Gun Metal PVD finish and steel shafts</li><li style=\"position: relative; padding: 3px 0px 3px 15px; list-style: none;\">100% graphite shafts in driver, fwy wood. 80% graphite shafts in hybrids. Stainless steel shafts in irons and putter.</li><li style=\"position: relative; padding: 3px 0px 3px 15px; list-style: none;\">TPX branded rubber grips</li><li style=\"position: relative; padding: 3px 0px 3px 15px; list-style: none;\">Stainless steel 35\" putter</li><li style=\"position: relative; padding: 3px 0px 3px 15px; list-style: none;\">Oversized Jumbo rubber putter grip</li></ul>','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d179.7579698681506!2d-79.03845417853977!3d43.8739094855817!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89d5200a4e2668ab%3A0x2b1acff7dde614a5!2s4%20Sharp%20Crescent%2C%20Ajax%2C%20ON%20L1T%203R1!5e0!3m2!1sen!2sca!4v1668277933926!5m2!1sen!2sca\" width=\"100%\" height=\"400\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>','',2,46,31,'testclient512@gmail.com','3333333333','L13TR1',10.00,50.00,'CAD','10am - 6pm',0,'golf kit hr 1.JPG','55482eb5-bf28-45cd-a5c6-0c72dd3c3e5c','Golf 2.JPG','00e957c2-c435-4095-b3f8-8769a423dedc','Golf 3.JPG','6aac0e04-4d4e-458e-b7cf-abc5ab1e5c66','Golf 4.JPG','71bb95dc-2610-453b-93d2-aff8899f6311',NULL,'',NULL,'',1,0,1,0,NULL,'2023-02-12 16:19:54',NULL,'2022-11-12 18:34:14'),
(69,27,28,74,'Sanmisha Sport','sanmisha-sport','Dombivali','<p>Lorem ipsum <b>dolor sit amet, consectetu</b>r adipiscing elit. Phasellus faucibus est ut massa aliquam fringilla. Duis luctus scelerisque iaculis. Suspendisse malesuada ipsum in justo venenatis malesuada. Mauris congue arcu magna, a ultrices justo iaculis in. Suspendisse potenti. Cras a eros est. Proin cursus odio sit amet sapien auctor sodales. In tempor massa sed dictum vulputate.<br><br>Etiam commodo est nisi, et luctus quam sollicitudin vel. Maecenas lobortis, sem in faucibus semper, orci magna sollicitudin leo, ac placerat turpis massa ut magna. Curabitur hendrerit lacus diam, ac semper purus mattis laoreet. Curabitur faucibus ullamcorper mattis. Donec vel augue ac lorem pretium laoreet. Aliquam erat volutpat. Integer ut felis vitae purus auctor luctus. Suspendisse fringilla consequat ullamcorper.<br><br></p>','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3767.48178917478!2d73.0895439150103!3d19.217823487006626!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be79592ca1a7983%3A0x69d8355cf2eb09e2!2sSanmisha%20Technologies%20Pvt.%20Ltd.!5e0!3m2!1sen!2sin!4v1671694234735!5m2!1sen!2sin\" width=\"100%\" height=\"400\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>','',2,40,20,'sanjeev@sanmisha.com','9820427417','421201',20.00,50.00,'','',0,'Best-golf-clubs-for-beginners-to-intermediate--1.jpg','4a34852f-ec79-4dc1-815e-e3331e8861f6',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',1,0,1,0,NULL,'2023-02-12 16:21:22',NULL,'2022-12-22 06:17:36'),
(70,26,32,75,'add','add','RAHUL NAGAR NO 2, PIPELINE,\r\nOPP PRIYADARSHINI sion trombay','','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3767.48178917478!2d73.0895439150103!3d19.217823487006626!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be79592ca1a7983%3A0x69d8355cf2eb09e2!2sSanmisha%20Technologies%20Pvt.%20Ltd.!5e0!3m2!1sen!2sin!4v1661338074438!5m2!1sen!2sin\" width=\"100%\" height=\"400\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>','',2,40,20,'vs10301030@gmail.com','8976931431','400022',12.00,NULL,'','{\"Monday\":[\"\",\"\"],\"Tuesday\":[\"\",\"\"],\"Wednesday\":[\"\",\"\"],\"Thursday\":[\"\",\"\"],\"Friday\":[\"\",\"\"],\"Saturday\":[\"\",\"\"],\"Sunday\":[\"\",\"\"]}',0,'bottleImg1.jpg','c7d92efb-642d-4293-9e4e-13e92b949ddc','imgBottle1.jpg','0d82f3f6-6572-412c-a7f8-79bffe456d26','presure washer.JPG','73b0f52c-a823-473d-bfde-d6d22d3c657f',NULL,'',NULL,'',NULL,'',1,0,0,0,NULL,'2023-04-06 09:47:45',NULL,'2023-01-19 08:17:16'),
(71,31,28,74,'Snow Mobile','snow-mobile','Chinook Mall, Calgary , Alberta','Our youth sled is the perfect way to introduce your kids into the sport with maximized rider safety, years of proven performance and a price point everyone can agree on. Stay out of the way Mom &amp; Dad -- this winter the kids are leading the way.','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2510.955412255661!2d-114.0755335842819!3d50.99849607955471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x537170558c817a7b%3A0xa80ab79074859579!2sCF%20Chinook%20Centre!5e0!3m2!1sen!2sca!4v1675578530628!5m2!1sen!2sca\" width=\"100%\" height=\"400\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>','',2,40,20,'ab5736451@gmail.com','1111111111','T2H0K8',10.00,50.00,'CAD','{\"Monday\":\"Closed\",\"Tuesday\":[\"11:15 am\",\"10:00 pm\"],\"Wednesday\":[\"11:00 am\",\"9:45 pm\"],\"Thursday\":[\"11:00 am\",\"6:15 pm\"],\"Friday\":[\"11:15 am\",\"12:00 pm\"],\"Saturday\":\"Closed\",\"Sunday\":\"Closed\"}',0,'Capture.JPG','54dabe8e-aa61-4ad3-94b6-64dcf4b6d7ce','snow mobile 2.JPG','2b777cb0-825a-440a-949c-2337ae7670c5','snow mobile 4.JPG','74f5c0d3-c947-4d52-84e1-bcaea7ace9a2',NULL,'',NULL,'',NULL,'',1,1,1,0,NULL,'2023-02-27 05:03:40',NULL,'2023-02-05 06:36:51'),
(72,4,36,NULL,'Luggage Carrier','luggage-carrier','Southcenter mall calgary','<p>Test</p>','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2513.4408023556925!2d-114.06844598425027!3d50.95255387954776!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x537176cc82d44277%3A0x48b3bbdc69c05da9!2sSouthcentre%20Mall!5e0!3m2!1sen!2sca!4v1678329237306!5m2!1sen!2sca\" width=\"100%\" height=\"400\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>',NULL,2,40,20,'anand.bhadekar@gmail.com','2222222222','T7D3R3',10.00,50.00,'CAD','{\"Monday\":[\"8:00 am\",\"9:00 am\"],\"Tuesday\":[\"8:15 am\",\"8:15 am\"],\"Wednesday\":\"Closed\",\"Thursday\":[\"9:00 am\",\"10:45 am\"],\"Friday\":[\"10:00 am\",\"12:15 pm\"],\"Saturday\":[\"9:00 am\",\"4:00 pm\"],\"Sunday\":[\"9:15 am\",\"5:00 pm\"]}',0,'thule.JPG','bb58ace8-386b-4889-8693-9ed1fcb7ef09',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',1,0,0,1,NULL,'2023-03-09 02:49:13',NULL,'2023-03-09 02:42:20'),
(73,35,32,NULL,'All purpose camping tent for wet and dry camping ','party-rental','test','Text text Text text Text text Text text \r\nText text Text text Text text Text text \r\nText text Text text Text text Text text \r\nText text Text text Text text Text text \r\n\r\nBox Box Box Box Box Box Box Box Box\r\nBox Box Box Box Box Box Box Box Box\r\nBox Box Box Box Box Box Box Box Box\r\nBox Box Box Box Box Box Box Box Box\r\n','',NULL,NULL,NULL,20,'tc223334@gmail.com','2222222222','',5.00,NULL,'','{\"Monday\":[\"\",\"\"],\"Tuesday\":[\"\",\"\"],\"Wednesday\":[\"\",\"\"],\"Thursday\":[\"\",\"\"],\"Friday\":\"Closed\",\"Saturday\":[\"8:45 am\",\"11:00 am\"],\"Sunday\":[\"9:15 am\",\"11:45 am\"]}',0,'custom logo light.JPG','230a9609-9f0e-4817-84fa-ba00046bdef4',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',1,0,0,0,NULL,'2023-03-09 03:52:14',NULL,'2023-03-09 03:12:39');

/*Table structure for table `listly` */

DROP TABLE IF EXISTS `listly`;

CREATE TABLE `listly` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `listing` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `review` varchar(255) DEFAULT NULL,
  `service_provider_review` varchar(255) DEFAULT NULL,
  `rating` varchar(50) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT 0.00,
  `sub_category` varchar(255) DEFAULT NULL,
  `banner` varchar(255) DEFAULT NULL,
  `banner_dir` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_dir` varchar(255) DEFAULT NULL,
  `service_provider_name` varchar(255) DEFAULT NULL,
  `blog` varchar(255) DEFAULT NULL,
  `payment_type` varchar(255) DEFAULT NULL,
  `host_name` varchar(255) DEFAULT NULL,
  `terms_abd_conditions` varchar(255) DEFAULT NULL,
  `about_us` varchar(255) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `listly` */

/*Table structure for table `messages` */

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `message_date` datetime DEFAULT NULL,
  `from_date` datetime DEFAULT NULL,
  `to_date` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

/*Data for the table `messages` */

insert  into `messages`(`id`,`message`,`sender_id`,`receiver_id`,`message_date`,`from_date`,`to_date`,`modified_by`,`modified`,`created_by`,`created`) values 
(1,'hifff',12,6,NULL,NULL,NULL,12,'2022-08-02 07:29:27',12,'2022-08-02 07:29:27'),
(2,'hifff',12,6,NULL,NULL,NULL,12,'2022-08-02 07:33:39',12,'2022-08-02 07:33:39'),
(3,'hi i am your service',12,6,NULL,NULL,NULL,12,'2022-08-02 07:44:04',12,'2022-08-02 07:44:04'),
(4,'hi i like your services\r\n',12,11,NULL,NULL,NULL,12,'2022-08-02 07:46:34',12,'2022-08-02 07:46:34'),
(5,'fxgfggf',12,6,NULL,NULL,NULL,12,'2022-08-02 10:33:41',12,'2022-08-02 10:33:41'),
(6,'gregg',12,11,NULL,NULL,NULL,12,'2022-08-02 10:34:47',12,'2022-08-02 10:34:47'),
(7,'good morning',12,11,NULL,NULL,NULL,12,'2022-08-02 10:40:48',12,'2022-08-02 10:40:48'),
(8,'good',12,11,NULL,NULL,NULL,12,'2022-08-02 10:43:07',12,'2022-08-02 10:43:07'),
(9,'hfh',12,11,NULL,NULL,NULL,12,'2022-08-02 10:44:20',12,'2022-08-02 10:44:20'),
(10,'final',12,11,NULL,NULL,NULL,12,'2022-08-02 10:45:27',12,'2022-08-02 10:45:27'),
(11,'final drafd',12,11,NULL,NULL,NULL,12,'2022-08-02 10:50:24',12,'2022-08-02 10:50:24'),
(12,'hi',12,11,NULL,NULL,NULL,12,'2022-08-02 10:53:42',12,'2022-08-02 10:53:42'),
(13,'check last one',12,11,NULL,NULL,NULL,12,'2022-08-02 10:55:10',12,'2022-08-02 10:55:10'),
(14,'Test Message to vendor',4,NULL,NULL,NULL,NULL,4,'2022-08-03 10:15:02',4,'2022-08-03 10:15:02'),
(15,'Can I get more information on rates and availibility',4,NULL,NULL,NULL,NULL,4,'2022-08-03 10:39:37',4,'2022-08-03 10:39:37'),
(16,'Test message',4,NULL,NULL,NULL,NULL,4,'2022-08-04 19:16:35',4,'2022-08-04 19:16:35'),
(17,'Test Query ',4,NULL,NULL,NULL,NULL,4,'2022-08-20 19:59:05',4,'2022-08-20 19:59:05'),
(18,'sdfsdfsdf',4,NULL,NULL,NULL,NULL,4,'2022-08-22 10:30:43',4,'2022-08-22 10:30:43'),
(19,'gfd',7,5,NULL,NULL,NULL,7,'2022-08-22 16:58:49',7,'2022-08-22 16:58:49'),
(20,'hi this is testing',7,NULL,NULL,NULL,NULL,7,'2022-08-22 16:59:18',7,'2022-08-22 16:59:18'),
(21,'hi test',7,NULL,NULL,NULL,NULL,7,'2022-08-22 18:40:29',7,'2022-08-22 18:40:29'),
(22,'Test Message',4,5,NULL,NULL,NULL,4,'2022-08-25 10:01:34',4,'2022-08-25 10:01:34'),
(23,'Test Message',4,5,NULL,NULL,NULL,4,'2022-08-25 10:21:25',4,'2022-08-25 10:21:25'),
(24,'Test query',4,NULL,NULL,NULL,NULL,4,'2022-08-29 22:48:48',4,'2022-08-29 22:48:48'),
(25,'hello can you please send further information',4,4,NULL,NULL,NULL,4,'2022-09-12 10:22:13',4,'2022-09-12 10:22:13'),
(26,'Can you pls provide more information on this lawn mover',17,17,NULL,NULL,NULL,17,'2022-09-20 13:13:40',17,'2022-09-20 13:13:40'),
(27,'Can you pls provide further information ',4,4,NULL,NULL,NULL,4,'2022-11-15 23:58:24',4,'2022-11-15 23:58:24'),
(28,'Test message ',4,4,NULL,NULL,NULL,4,'2022-12-29 07:44:16',4,'2022-12-29 07:44:16'),
(29,'Test message to vendor',4,19,NULL,NULL,NULL,4,'2023-01-01 14:32:36',4,'2023-01-01 14:32:36'),
(30,'Test msg 3',4,19,NULL,NULL,NULL,4,'2023-01-01 14:42:48',4,'2023-01-01 14:42:48'),
(31,'Test msg',4,19,NULL,NULL,NULL,4,'2023-01-01 14:43:03',4,'2023-01-01 14:43:03'),
(32,'Test msg',4,19,NULL,NULL,NULL,4,'2023-01-01 14:43:12',4,'2023-01-01 14:43:12'),
(33,'Test msgTest msg Test msg',4,19,NULL,NULL,NULL,4,'2023-01-01 14:44:51',4,'2023-01-01 14:44:51'),
(34,'',24,4,NULL,NULL,NULL,24,'2023-01-04 05:32:58',24,'2023-01-04 05:32:58'),
(35,'Test message',4,17,NULL,NULL,NULL,4,'2023-01-13 08:42:32',4,'2023-01-13 08:42:32'),
(42,'zasfsd',29,26,NULL,'2023-01-02 00:00:00','2023-09-02 00:00:00',29,'2023-02-01 10:42:28',29,'2023-02-01 10:42:28'),
(43,'sdsa',29,26,NULL,'2023-01-02 00:00:00','2023-02-02 00:00:00',29,'2023-02-01 11:06:45',29,'2023-02-01 11:06:45'),
(44,'sxcx',29,26,NULL,'2023-01-02 00:00:00','2023-02-01 00:00:00',29,'2023-02-01 11:09:00',29,'2023-02-01 11:09:00'),
(45,'sxcx',29,26,NULL,'2023-01-02 00:00:00','2023-01-02 00:00:00',29,'2023-02-01 11:13:59',29,'2023-02-01 11:13:59'),
(46,'jokj',29,26,NULL,'2023-01-02 00:00:00','2023-01-02 00:00:00',29,'2023-02-01 11:14:54',29,'2023-02-01 11:14:54'),
(47,'uui',29,26,NULL,'2023-01-02 00:00:00','2023-01-02 00:00:00',29,'2023-02-01 11:19:47',29,'2023-02-01 11:19:47'),
(48,'jkdhnfshkj\r\n',29,26,NULL,'2023-02-01 00:00:00','2023-02-10 00:00:00',29,'2023-02-01 11:39:44',29,'2023-02-01 11:39:44'),
(49,'ihim',29,26,NULL,'2023-02-01 00:00:00','2023-02-03 00:00:00',29,'2023-02-01 11:42:28',29,'2023-02-01 11:42:28'),
(50,'I want this service',29,26,NULL,'2023-02-01 00:00:00','2023-02-03 00:00:00',29,'2023-02-01 11:43:02',29,'2023-02-01 11:43:02'),
(51,'iuhiu',29,26,NULL,'2023-02-01 00:00:00','2023-02-01 00:00:00',29,'2023-02-01 11:45:50',29,'2023-02-01 11:45:50'),
(52,'Hello ',4,4,NULL,'2023-02-06 00:00:00','2023-02-06 00:00:00',4,'2023-02-02 00:37:49',4,'2023-02-02 00:37:49'),
(53,'test',5,4,NULL,'2023-02-02 00:00:00','2023-02-02 00:00:00',5,'2023-02-03 05:42:15',5,'2023-02-03 05:42:15'),
(54,'Hello test message ',5,4,NULL,'2023-02-03 00:00:00','2023-02-03 00:00:00',5,'2023-02-03 17:27:31',5,'2023-02-03 17:27:31'),
(55,'Test 2',5,4,NULL,'2023-02-03 00:00:00','2023-02-10 00:00:00',5,'2023-02-03 17:29:22',5,'2023-02-03 17:29:22'),
(56,'',5,31,NULL,'2023-02-04 00:00:00','2023-02-06 00:00:00',5,'2023-02-05 06:56:01',5,'2023-02-05 06:56:01'),
(57,'Hello, is this snowmobile available for rent over next weekend. Do you deliver?',5,31,NULL,'2023-02-11 00:00:00','2023-02-12 00:00:00',5,'2023-02-05 07:00:43',5,'2023-02-05 07:00:43'),
(58,'Hello , test message',17,31,NULL,'2023-02-11 00:00:00','2023-02-12 00:00:00',17,'2023-02-05 07:17:52',17,'2023-02-05 07:17:52'),
(59,'Hi, please contact me if it’s available!!',5,17,NULL,'2023-02-10 00:00:00','2023-02-10 00:00:00',5,'2023-02-11 06:05:25',5,'2023-02-11 06:05:25'),
(60,'Hi, please contact me if it’s available!!',4,4,NULL,'2025-02-02 00:00:00','2025-02-02 00:00:00',4,'2023-02-26 19:42:36',4,'2023-02-26 19:42:36'),
(61,'Hi, please contact me if it’s available!!',4,17,NULL,'2025-02-02 00:00:00','2025-02-02 00:00:00',4,'2023-02-26 22:17:40',4,'2023-02-26 22:17:40'),
(62,'Hi, please contact me if it’s available!!',4,17,NULL,'2025-02-02 00:00:00','2025-02-02 00:00:00',4,'2023-02-26 22:19:08',4,'2023-02-26 22:19:08'),
(63,'Hi, please contact me if it’s available!!',4,17,NULL,'2023-03-01 00:00:00','2023-03-01 00:00:00',4,'2023-02-26 22:19:28',4,'2023-02-26 22:19:28'),
(64,'Hello is this available ',4,17,NULL,'2023-03-01 00:00:00','2023-03-01 00:00:00',4,'2023-02-26 22:20:32',4,'2023-02-26 22:20:32'),
(65,'Hi, please contact me if it’s available!!',29,31,NULL,'2025-02-03 00:00:00','2025-02-03 00:00:00',29,'2023-02-27 05:05:12',29,'2023-02-27 05:05:12'),
(66,'Hi, please contact me if it’s available!!',30,17,NULL,'2023-03-04 00:00:00','2023-03-04 00:00:00',30,'2023-03-05 01:53:36',30,'2023-03-05 01:53:36'),
(67,'hello test message',30,17,NULL,'2023-03-04 00:00:00','2023-03-04 00:00:00',30,'2023-03-05 01:54:29',30,'2023-03-05 01:54:29'),
(68,'Hi, please contact me if it’s available!!',36,4,NULL,'2023-03-08 00:00:00','2023-03-08 00:00:00',36,'2023-03-09 02:15:45',36,'2023-03-09 02:15:45'),
(69,'Hi, please contact me if it’s available!!',35,4,NULL,'2023-03-08 00:00:00','2023-03-08 00:00:00',35,'2023-03-09 02:52:32',35,'2023-03-09 02:52:32'),
(70,'Hi, please contact me if it’s available!!',35,4,NULL,'2024-03-01 00:00:00','2024-03-01 00:00:00',35,'2023-03-09 02:52:58',35,'2023-03-09 02:52:58');

/*Table structure for table `packages` */

DROP TABLE IF EXISTS `packages`;

CREATE TABLE `packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `package` varchar(255) DEFAULT NULL,
  `no_of_listings` tinyint(4) DEFAULT NULL,
  `period` tinyint(4) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `packages` */

insert  into `packages`(`id`,`package`,`no_of_listings`,`period`,`active`,`modified_by`,`modified`,`created_by`,`created`) values 
(1,'Golden Package (Business)',10,12,0,1,'2023-03-15 05:09:30',1,'2021-03-22 12:21:06'),
(2,'Silver Package ',4,9,0,1,'2023-03-15 05:09:21',1,'2021-03-22 15:01:22'),
(3,'Bronze Package',2,6,1,1,'2023-02-12 16:06:32',1,'2021-03-31 10:39:03');

/*Table structure for table `pages` */

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `menu` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `page_title` varchar(255) DEFAULT NULL,
  `page_heading` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `meta_tags` text DEFAULT NULL,
  `layout` varchar(100) DEFAULT 'default',
  `body` mediumtext DEFAULT NULL,
  `hidden` tinyint(1) DEFAULT 0,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `pages` */

insert  into `pages`(`id`,`parent_id`,`lft`,`rght`,`level`,`menu`,`slug`,`page_title`,`page_heading`,`keywords`,`description`,`meta_tags`,`layout`,`body`,`hidden`,`created`,`modified`) values 
(1,NULL,1,4,0,'Home','home','','Rent what you need!','','Discover Shops, Services & Freelancers around Canada','','default','<p><br></p>',0,'2019-09-25 12:22:36','2023-03-30 12:19:29'),
(2,NULL,5,8,0,'About Us','about-us','About Us','About Us','About Us','About Us ','','display','<p>This is test content</p><p><strong></strong></p>',0,'2019-09-25 12:22:49','2023-03-30 12:19:44'),
(3,2,6,7,1,'About Company','about-company','','','','','','default','',0,'2019-09-25 12:29:17','2019-09-25 12:29:51'),
(4,NULL,13,14,0,'test','test','','','','','','default','',0,'2019-09-25 13:09:30','2020-02-05 10:37:21'),
(5,NULL,9,10,0,'contact-us','contact-us','','','','','','default','<div class=\"container\">\r\n<div class=\"row\">\r\n<h3 class=\"text-center pt-5 pb-5\"\">Contact Us</h3>\r\n</div>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-sm-5\">\r\n<div class=\"row contact_details\">\r\n<div class=\"media\">\r\n<div class=\"d-flex\"><i aria-hidden=\"true\" class=\"fa fa-map-marker\"></i></div>\r\n\r\n<div class=\"media-body\">\r\n<p>House # 402, Roboto Street,<br />\r\nNew York, USA.</p>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"row contact_details\">\r\n<div class=\"media\">\r\n<div class=\"d-flex\"><i aria-hidden=\"true\" class=\"fa fa-phone\"></i></div>\r\n\r\n<div class=\"media-body\"><a href=\"tel:+1109171234567\">+110 - 917 - 123 - 4567</a> <a href=\"tel:+1101911897654\">+110 - 191 - 189 - 7654</a></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"row contact_details\">\r\n<div class=\"media\">\r\n<div class=\"d-flex\"><i aria-hidden=\"true\" class=\"fa fa-envelope\"></i></div>\r\n\r\n<div class=\"media-body\"><a href=\"mailto:busines@persuit.com\">busines@persuit.com</a> <a href=\"mailto:support@persuit.com\">support@persuit.com</a></div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-sm-7\">\r\n<form action=\"contact_process.php\" class=\"contact_us_form row\" id=\"contactForm\" method=\"post\" novalidate=\"novalidate\">\r\n<div class=\"form-group col-lg-4\"><input class=\"form-control\" id=\"name\" name=\"name\" placeholder=\"Full Name *\" type=\"text\" /></div>\r\n\r\n<div class=\"form-group col-lg-4\"><input class=\"form-control\" id=\"email\" name=\"email\" placeholder=\"Email Address *\" type=\"email\" /></div>\r\n\r\n<div class=\"form-group col-lg-4\"><input class=\"form-control\" id=\"website\" name=\"website\" placeholder=\"Your Website\" type=\"text\" /></div>\r\n\r\n<div class=\"form-group col-lg-12\"><textarea class=\"form-control\" id=\"message\" name=\"message\" placeholder=\"Type Your Message...\" rows=\"1\"></textarea></div>\r\n\r\n<div class=\"form-group col-lg-12\"><button class=\"btn update_btn form-control\" type=\"submit\" value=\"submit\">Send Message</button></div>\r\n</form>\r\n</div>\r\n</div>\r\n</div>\r\n',0,'2019-09-26 10:52:40','2020-02-05 16:37:11'),
(6,NULL,11,12,0,'upcoming offers','upcoming-offers','','','','','','default','<section class=\"from_blog_area\">\r\n<div class=\"container\">\r\n<div class=\"from_blog_inner\">\r\n<div class=\"c_main_title\">\r\n<h2 style=\"color:#433838\">Upcoming Offers</h2>\r\n</div>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-sm-4 mt-3\">\r\n<div class=\"card\"><img alt=\"Card image cap\" class=\"card-img-top\" src=\"/files/advertisements/image_1/e2de08f3-0ed5-4b41-8798-f250b88df993/big_adv.jpg\" /></div>\r\n</div>\r\n\r\n<div class=\"col-sm-4 mt-3\">\r\n<div class=\"card\"><img alt=\"Card image cap\" class=\"card-img-top\" src=\"/files/advertisements/image_1/e2de08f3-0ed5-4b41-8798-f250b88df993/big_adv.jpg\" /></div>\r\n</div>\r\n\r\n<div class=\"col-sm-4 mt-3\">\r\n<div class=\"card\"><img alt=\"Card image cap\" class=\"card-img-top\" src=\"/files/advertisements/image_1/e2de08f3-0ed5-4b41-8798-f250b88df993/big_adv.jpg\" /></div>\r\n</div>\r\n\r\n<div class=\"col-sm-4 mt-3\">\r\n<div class=\"card\"><img alt=\"Card image cap\" class=\"card-img-top\" src=\"/files/advertisements/image_1/e2de08f3-0ed5-4b41-8798-f250b88df993/big_adv.jpg\" /></div>\r\n</div>\r\n\r\n<div class=\"col-sm-4 mt-3\">\r\n<div class=\"card\"><img alt=\"Card image cap\" class=\"card-img-top\" src=\"/files/advertisements/image_1/e2de08f3-0ed5-4b41-8798-f250b88df993/big_adv.jpg\" /></div>\r\n</div>\r\n\r\n<div class=\"col-sm-4 mt-3\">\r\n<div class=\"card\"><img alt=\"Card image cap\" class=\"card-img-top\" src=\"/files/advertisements/image_1/e2de08f3-0ed5-4b41-8798-f250b88df993/big_adv.jpg\" /></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n',0,'2020-02-04 18:13:42','2020-02-05 12:16:07'),
(7,NULL,15,16,0,'trending-offers','trending-offers','','','','','','default','<section class=\"from_blog_area\">\r\n<div class=\"container\">\r\n<div class=\"from_blog_inner\">\r\n<div class=\"c_main_title\">\r\n<h2 style=\"color:#433838\">Trending Offers</h2>\r\n</div>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-sm-4 mt-3\">\r\n<div class=\"card\"><img alt=\"Card image cap\" class=\"card-img-top\" src=\"/files/advertisements/image_1/e534010b-0906-4fb0-b265-75a5ce38ccf2/small_adv.jpg\" /></div>\r\n</div>\r\n\r\n<div class=\"col-sm-4 mt-3\">\r\n<div class=\"card\"><img alt=\"Card image cap\" class=\"card-img-top\" src=\"/files/advertisements/image_1/e534010b-0906-4fb0-b265-75a5ce38ccf2/small_adv.jpg\" /></div>\r\n</div>\r\n\r\n<div class=\"col-sm-4 mt-3\">\r\n<div class=\"card\"><img alt=\"Card image cap\" class=\"card-img-top\" src=\"/files/advertisements/image_1/e534010b-0906-4fb0-b265-75a5ce38ccf2/small_adv.jpg\" /></div>\r\n</div>\r\n\r\n<div class=\"col-sm-4 mt-3\">\r\n<div class=\"card\"><img alt=\"Card image cap\" class=\"card-img-top\" src=\"/files/advertisements/image_1/e534010b-0906-4fb0-b265-75a5ce38ccf2/small_adv.jpg\" /></div>\r\n</div>\r\n\r\n<div class=\"col-sm-4 mt-3\">\r\n<div class=\"card\"><img alt=\"Card image cap\" class=\"card-img-top\" src=\"/files/advertisements/image_1/e534010b-0906-4fb0-b265-75a5ce38ccf2/small_adv.jpg\" /></div>\r\n</div>\r\n\r\n<div class=\"col-sm-4 mt-3\">\r\n<div class=\"card\"><img alt=\"Card image cap\" class=\"card-img-top\" src=\"/files/advertisements/image_1/e534010b-0906-4fb0-b265-75a5ce38ccf2/small_adv.jpg\" /></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n',0,'2020-02-05 11:36:08','2020-02-05 16:42:21'),
(8,NULL,17,18,0,'Terms and Conditions','terms-and-conditions','Terms and Conditions','Terms and Conditions','','Sample Terms and Conditions','','display','<p><strong>Terms and Conditions</strong></p>\r\n',0,'2020-02-05 12:39:11','2023-02-12 18:12:56'),
(9,NULL,2,3,1,'Privacy Policy','privacy-policy','','','','Privacy Policy\r\nPrivacy Policy\r\nPrivacy Policy\r\nPrivacy Policy\r\nPrivacy Policy\r\nPrivacy Policy','','display','<div class=\"container \">\r\n<div class=\"row \">\r\n<h3 class=\"text-center pt-5 pb-5\">Privacy Policy</h3>\r\n</div>\r\n\r\n<div class=\"row mb-5 text-justify\">\r\n<p>Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come HereText Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come HereText Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here</p>\r\n\r\n<p>Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come HereText Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come HereText Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here Text Will Come Here</p>\r\n</div>\r\n</div>\r\n',0,'2020-02-05 12:49:51','2023-02-20 05:34:50');

/*Table structure for table `phinxlog` */

DROP TABLE IF EXISTS `phinxlog`;

CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `phinxlog` */

insert  into `phinxlog`(`version`,`migration_name`,`start_time`,`end_time`,`breakpoint`) values 
(20210216062752,'Initial','2021-02-16 11:57:52','2021-02-16 11:57:52',0),
(20210216063413,'ApplicationTable','2021-02-16 12:04:13','2021-02-16 12:04:13',0),
(20210216063550,'Dump','2021-02-16 12:06:37','2021-02-16 12:06:37',0),
(20210216064006,'Application','2021-02-16 12:10:33','2021-02-16 12:10:33',0),
(20210216064040,'Application2','2021-02-16 12:10:40','2021-02-16 12:10:40',0),
(20210216064136,'Application3','2021-02-16 12:11:36','2021-02-16 12:11:36',0),
(20210216064311,'ApplicationDob','2021-02-16 12:13:11','2021-02-16 12:13:11',0);

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `product` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `photo_dir` varchar(255) DEFAULT NULL,
  `populer` tinyint(1) DEFAULT NULL,
  `list_order` varchar(3) DEFAULT NULL,
  `popular` tinyint(1) DEFAULT 0,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `products` */

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `alias` varchar(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`alias`,`created`,`modified`) values 
(1,'root','root',NULL,NULL),
(2,'member','member',NULL,NULL),
(5,'vendor','vendor',NULL,NULL);

/*Table structure for table `roles_users` */

DROP TABLE IF EXISTS `roles_users`;

CREATE TABLE `roles_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

/*Data for the table `roles_users` */

insert  into `roles_users`(`id`,`user_id`,`role_id`) values 
(1,1,1),
(5,6,2),
(6,7,2),
(8,9,2),
(15,16,2),
(16,17,2),
(18,19,2),
(20,21,2),
(21,22,2),
(22,23,2),
(23,24,2),
(24,25,2),
(25,26,2),
(29,31,2),
(32,34,2),
(33,35,2),
(37,39,2),
(38,40,2),
(39,41,2),
(40,42,2),
(41,43,2),
(42,44,2),
(43,45,2),
(44,47,2),
(45,48,2),
(46,2,2),
(48,4,2),
(49,5,2),
(50,8,2),
(51,10,2),
(52,11,2),
(53,12,2),
(54,13,2),
(55,14,2),
(56,15,2),
(57,18,2),
(58,20,2),
(59,27,2),
(60,28,2),
(61,29,2),
(62,30,2),
(63,32,2),
(64,33,2),
(65,36,2),
(66,37,2),
(67,38,2);

/*Table structure for table `sections` */

DROP TABLE IF EXISTS `sections`;

CREATE TABLE `sections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT NULL,
  `section` varchar(100) DEFAULT NULL,
  `contents` mediumtext DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

/*Data for the table `sections` */

insert  into `sections`(`id`,`page_id`,`section`,`contents`,`modified`,`created`) values 
(15,1,'section2','abcd','2019-09-25 11:55:33','2019-09-25 11:55:33'),
(20,NULL,'footer-3','<h3 data-target=\"#collapse_ft_3\">Contacts</h3>\r\n					<div class=\"collapse dont-collapse-sm\" id=\"collapse_ft_3\">\r\n						<ul class=\"contacts\">\r\n							<li><i class=\"ti-headphone-alt\"></i><a href=\"tel:+91 99234 42344\">+91 99234 42344</a></li>\r\n							<li><i class=\"ti-email\"></i><a href=\"mailto:abc@abc.com\">abc@abc.com</a></li>\r\n						</ul>\r\n					</div>','2022-06-24 12:18:55','2019-11-08 17:53:09'),
(21,NULL,'footer-4','<div class=\"follow_us\">\r\n							<ul>\r\n								<li><a href=\"#0\"><i class=\"ti-facebook\"></i></a></li>\r\n								<li><a href=\"#0\"><i class=\"ti-twitter-alt\"></i></a></li>\r\n								<li><a href=\"#0\"><i class=\"ti-pinterest\"></i></a></li>\r\n								<li><a href=\"#0\"><i class=\"ti-instagram\"></i></a></li>\r\n							</ul>\r\n						</div>','2022-06-24 12:19:40','2019-11-08 17:54:18'),
(22,NULL,'footer-1','<h3 data-target=\"#collapse_ft_1\">Quick Links</h3>\r\n					<div class=\"collapse dont-collapse-sm\" id=\"collapse_ft_1\">\r\n						<ul class=\"links\">\r\n							<li><a href=\"/about-us\">About us</a></li>\r\n							<li><a href=\"/terms-of-service\">Terms of Service</a></li>\r\n						</ul>\r\n					</div>','2022-06-24 12:18:22','2020-01-06 17:54:37'),
(23,NULL,'testimonials','<div class=\"item\">\r\n					<div class=\"strip grid\">\r\n						<div class=\"wrapper\">\r\n							<p><i class=\"ti ti-quote-left\" style=\"font-size: 24px;color: #ed2025;\"></i> Text will come here Text will come here Text will come here Text will come here Text will come here Text will come here Text will come here Text will come here Text will come here Text will come here Text will come here Text will come here </p>\r\n							<h6><strong>Name</strong> - Location</h6>\r\n						</div>\r\n						\r\n					</div>\r\n				</div>\r\n				<!-- /item -->','2022-06-24 12:14:47','2020-01-06 17:55:22'),
(27,NULL,'footer-2','<h3 data-target=\"#collapse_ft_2\">Categories</h3>\r\n					<div class=\"collapse dont-collapse-sm\" id=\"collapse_ft_2\">\r\n						<ul class=\"links\">\r\n							<!--?php foreach($this--->Ctgy-&gt;getFCategory() as $i =&gt; $category){?&gt;\r\n							    <li><a href=\"#0\"><!--?php echo $this--->Html-&gt;link($category, array(\'controller\' =&gt; \'listings\',\'action\'=&gt;\'index\',$i )); ?&gt;</a></li>\r\n							<!--?php } ?-->\r\n						</ul>\r\n					</div>','2022-06-24 12:18:42','2022-06-24 12:16:24'),
(28,NULL,'how_it_works-1','Cum doctus civibus efficiantur in imperdiet deterruisset.','2023-03-30 13:19:51','2023-03-30 12:06:07'),
(29,NULL,'how_it_works-2','<div style=\"line-height: 19px;\"><span style=\"font-size: 1rem;\">An nec placerat repudiare scripserit, temporibus complectitur at sea, vel ignota fierent eloquentiam id.</span><br></div>','2023-03-30 13:31:42','2023-03-30 12:44:21'),
(30,NULL,'how_it_works-3','<div style=\"line-height: 19px;\"><span style=\"font-size: 1rem;\">An nec placerat repudiare scripserit, temporibus complectitur at sea, vel ignota fierent eloquentiam id.</span><br></div>','2023-03-30 13:31:32','2023-03-30 12:44:50'),
(31,NULL,'how_it_works-4','<div style=\"line-height: 19px;\"><span style=\"font-size: 1rem;\">An nec placerat repudiare scripserit, temporibus complectitur at sea, vel ignota fierent eloquentiam id.</span><br></div>','2023-03-30 13:31:20','2023-03-30 12:45:09');

/*Table structure for table `states` */

DROP TABLE IF EXISTS `states`;

CREATE TABLE `states` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

/*Data for the table `states` */

insert  into `states`(`id`,`country_id`,`state`,`modified_by`,`modified`,`created_by`,`created`) values 
(40,2,'Alberta',1,'2022-04-02 10:20:53',1,'2022-04-02 10:20:53'),
(45,2,'British Columbia',1,'2022-09-11 21:56:55',1,'2022-09-11 21:56:55'),
(46,2,'Ontario',1,'2022-11-12 18:30:40',1,'2022-11-12 18:30:40');

/*Table structure for table `sub_categories` */

DROP TABLE IF EXISTS `sub_categories`;

CREATE TABLE `sub_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `sub_category` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `photo_dir` varchar(255) DEFAULT NULL,
  `populer` tinyint(1) DEFAULT NULL,
  `list_order` varchar(3) DEFAULT NULL,
  `popular` tinyint(1) DEFAULT 0,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=latin1;

/*Data for the table `sub_categories` */

insert  into `sub_categories`(`id`,`category_id`,`sub_category`,`slug`,`description`,`photo`,`photo_dir`,`populer`,`list_order`,`popular`,`modified`,`modified_by`,`created_by`,`created`) values 
(74,28,'Winter Sports','Nothing but best',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL),
(75,32,'Wedding','wedding','Rent everything you need for Weddings!!','Weddings.JPG','d10567fe-34a5-444c-a0c7-cbee975d2701',NULL,'',1,'2022-09-12 00:30:42',NULL,NULL,'2022-09-12 00:30:42'),
(76,32,'Furniture','furniture','Furniture for party rentals','Chairs and tables.JPG','ea4a8631-5ad5-434c-a1be-d3b53f300a0b',NULL,'',1,'2022-09-12 00:49:57',NULL,NULL,'2022-09-12 00:49:57'),
(77,31,'Camping Tents','camping-tents','\r\n','Camping 1.JPG','103be2f4-da83-4321-8fa8-ff99439badc7',NULL,'',1,'2022-09-13 07:12:20',NULL,NULL,'2022-09-13 07:12:20'),
(78,30,'Power Tools','power-tools','Power Tools','Products.JPG','c91ffc64-2f29-400b-8396-359499cfab37',NULL,'5',1,'2022-09-19 05:26:37',NULL,NULL,'2022-09-19 05:26:37'),
(79,35,'Summer Essentials','summer-essentials','','Categories.JPG','b47b1796-4f7d-4e5e-a349-62119c186477',NULL,'',1,'2022-09-19 05:31:19',NULL,NULL,'2022-09-19 05:31:19'),
(80,28,'Bikes','bikes','','Products.JPG','e0676009-1ac6-49f3-bc4f-20dd3c3f5e17',NULL,'',0,'2022-09-19 05:38:48',NULL,NULL,'2022-09-19 05:38:48'),
(81,30,'Gardening Tools','gardening-tools','Gardening Tools ','Categories.JPG','9fe875f6-3d22-42cc-a733-a90bbefd44b2',NULL,'',0,'2022-09-20 13:02:48',NULL,NULL,'2022-09-20 13:02:48'),
(82,34,'Cleaning Supplies','cleaning-supplies','Household cleaning supplies','household cleaning.JPG','8177fb72-716f-4d90-ab86-269e7d82a88b',NULL,'',1,'2022-11-12 17:04:08',NULL,NULL,'2022-11-12 17:04:08'),
(83,28,'Golf','golf','Golfing Essentials','golfing banner.JPG','0747a35d-fc61-4e8e-a194-a7fb7e25ea33',NULL,'',1,'2022-11-12 18:21:28',NULL,NULL,'2022-11-12 18:21:28'),
(84,36,'Bags','bags','bags','bags2.JPG','328a7bf6-7cf4-480e-b8d7-975b67cd4ea9',NULL,'7',1,'2023-02-12 16:05:07',NULL,NULL,'2023-02-12 16:05:07');

/*Table structure for table `subscriptions` */

DROP TABLE IF EXISTS `subscriptions`;

CREATE TABLE `subscriptions` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `registration_date` date DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `no_of_listings` tinyint(4) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 0,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `subscriptions` */

insert  into `subscriptions`(`id`,`user_id`,`package_id`,`registration_date`,`expiry_date`,`no_of_listings`,`active`,`modified_by`,`modified`,`created_by`,`created`) values 
(2,2,1,'2021-06-04','2022-06-04',3,1,NULL,'2021-06-04 15:28:00',NULL,'2021-06-04 15:28:00'),
(3,3,3,'2021-06-09','2022-06-09',1,1,NULL,'2021-06-09 11:00:48',NULL,'2021-06-09 11:00:48'),
(4,4,3,'2022-04-02','2023-04-02',1,1,1,'2022-04-02 10:56:51',1,'2022-04-02 10:56:51'),
(5,5,1,'2022-04-14','2023-04-14',3,0,NULL,'2022-04-14 09:43:37',NULL,'2022-04-14 09:43:37'),
(6,6,1,'2022-06-25','2023-06-25',3,1,NULL,'2022-06-25 10:25:00',NULL,'2022-06-25 10:25:00'),
(7,7,1,'2022-06-25','2023-06-25',3,1,NULL,'2022-06-25 10:26:32',NULL,'2022-06-25 10:26:32'),
(9,9,2,'2022-07-11','2022-12-11',2,1,NULL,'2022-07-11 18:05:35',NULL,'2022-07-11 18:05:35'),
(16,16,2,'2022-08-23','2023-01-23',2,1,NULL,'2022-08-23 12:44:15',NULL,'2022-08-23 12:44:15'),
(17,17,1,'2022-09-19','2023-03-19',7,1,NULL,'2022-09-19 06:09:55',NULL,'2022-09-19 06:09:55'),
(19,19,1,'2022-11-12','2023-05-12',7,1,1,'2023-03-15 07:46:39',NULL,'2022-11-12 16:34:40'),
(21,21,1,'2022-11-28','2023-05-28',7,0,1,'2023-03-15 07:46:15',NULL,'2022-11-28 04:35:25'),
(22,22,1,'2022-11-30','2023-05-30',7,1,NULL,'2022-11-30 05:19:19',NULL,'2022-11-30 05:19:18'),
(23,23,1,'2022-11-30','2023-05-30',7,1,NULL,'2022-11-30 05:31:13',NULL,'2022-11-30 05:31:13'),
(24,24,1,'2022-11-30','2023-05-30',7,1,NULL,'2022-11-30 05:42:46',NULL,'2022-11-30 05:42:46'),
(25,25,1,'2022-11-30','2023-05-30',7,1,NULL,'2022-11-30 05:48:59',NULL,'2022-11-30 05:48:59'),
(26,26,3,'2023-03-15','2023-09-15',2,1,NULL,'2023-03-15 13:57:59',NULL,'2022-11-30 11:56:30'),
(27,27,1,'2022-12-22','2023-06-22',7,1,NULL,'2022-12-22 06:13:55',NULL,'2022-12-22 06:13:55'),
(29,NULL,2,'2023-03-17','2023-12-17',4,1,29,'2023-03-17 08:23:10',29,'2023-03-17 08:23:10'),
(31,31,1,'2023-02-05','2023-08-05',7,1,NULL,'2023-02-05 06:14:34',NULL,'2023-02-05 06:14:34'),
(34,34,1,'2023-02-21','2023-11-21',4,1,NULL,'2023-02-21 03:49:14',NULL,'2023-02-21 03:49:14'),
(35,35,1,'2023-02-21','2023-11-21',4,1,NULL,'2023-02-21 03:54:53',NULL,'2023-02-21 03:54:53'),
(43,NULL,1,'2023-04-13','2024-04-13',10,1,1,'2023-04-13 07:19:23',NULL,'2023-04-13 07:14:06');

/*Table structure for table `tiny_auth_acl_rules` */

DROP TABLE IF EXISTS `tiny_auth_acl_rules`;

CREATE TABLE `tiny_auth_acl_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(2) NOT NULL,
  `role` varchar(50) NOT NULL,
  `path` varchar(250) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `path` (`path`,`role`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tiny_auth_acl_rules` */

insert  into `tiny_auth_acl_rules`(`id`,`type`,`role`,`path`,`created`,`modified`) values 
(1,1,'root','TinyAuthBackend.Auth::*','2021-02-26 11:05:37','2021-02-26 11:05:37'),
(2,1,'root','TinyAuthBackend.Allow::*','2021-02-26 11:05:37','2021-02-26 11:05:37'),
(3,1,'root','TinyAuthBackend.Acl::*','2021-02-26 11:05:37','2021-02-26 11:05:37'),
(4,1,'*','Admin/Dashboard::*','2021-02-26 11:09:35','2021-02-27 17:19:15');

/*Table structure for table `tiny_auth_allow_rules` */

DROP TABLE IF EXISTS `tiny_auth_allow_rules`;

CREATE TABLE `tiny_auth_allow_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(2) NOT NULL,
  `path` varchar(250) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `path` (`path`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tiny_auth_allow_rules` */

insert  into `tiny_auth_allow_rules`(`id`,`type`,`path`,`created`,`modified`) values 
(1,1,'Applications::apply','2021-02-26 11:21:43','2021-02-26 11:21:43');

/*Table structure for table `tiny_auth_backend_phinxlog` */

DROP TABLE IF EXISTS `tiny_auth_backend_phinxlog`;

CREATE TABLE `tiny_auth_backend_phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tiny_auth_backend_phinxlog` */

insert  into `tiny_auth_backend_phinxlog`(`version`,`migration_name`,`start_time`,`end_time`,`breakpoint`) values 
(20191027092406,'TinyAuthBackendDefault','2021-02-26 11:05:36','2021-02-26 11:05:36',0);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(25) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `activation_key` varchar(255) DEFAULT NULL,
  `api_key_plain` varchar(255) DEFAULT NULL,
  `api_key` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `is_vendor` tinyint(1) DEFAULT 0,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`mobile`,`password`,`activation_key`,`api_key_plain`,`api_key`,`active`,`is_vendor`,`created`,`modified`) values 
(1,'root','root@listly.com',NULL,'$2y$10$YzWKCsKp7Fim/rYaZsKkV.Vdb8b6DKq8NFlrYgkgsT8HAbjgwcYzy',NULL,NULL,NULL,1,1,NULL,'2023-03-15 07:49:54'),
(2,'Kamlesh Gavali','gavalikamlesh@gmail.com','8806891737','$2y$10$fzbJhsbEbGFr/fhgmIrlXO8F0MNKmKhSzNGHGnKEKVHN4XSeyfBUS','66d45b0d9fbde4e0ff97ec4d96a5ad9b8846b39c',NULL,NULL,1,1,'2021-06-04 15:28:00','2023-04-13 06:33:24'),
(4,'anand','anand.bhadekar@gmail.com','4169953879','$2y$10$4rlBX0B/ZwUU7n19EXGoT.qvNRM8scIx2.1nInm6dmi3SuY5WUanq','513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,1,1,'2022-04-02 10:56:51','2023-04-13 06:33:28'),
(5,'anand bhadekar','anaghapotdar09@gmail.com','1234567890','$2y$10$YzWKCsKp7Fim/rYaZsKkV.Vdb8b6DKq8NFlrYgkgsT8HAbjgwcYzy','66d45b0d9fbde4e0ff97ec4d96a5ad9b8846b39c',NULL,NULL,1,1,'2022-04-14 09:43:37','2022-05-12 11:02:33'),
(8,'Sanjeev Jayant Divekkar','sanjeev@sanmisha.com','9820427416','$2y$10$aW24/fwhx/sIbWuiIm146.d67U2pM8IKM2Yw7eARWcz2T6zLnM2GS','513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,1,1,'2022-07-07 16:55:44','2023-02-08 05:47:05'),
(9,'Andy Bob','andyb6151@gmail.com','6472176447','$2y$10$3T8op1rMEpMFOVT.DcK0P.Bf0n3luOxNkGNo3RRGW9YnGl5r9q8FS','66d45b0d9fbde4e0ff97ec4d96a5ad9b8846b39c',NULL,NULL,0,1,'2022-07-11 18:05:35','2022-07-11 18:07:01'),
(10,'Sanjeev Divekar','sanjeevdivekar@gmail.com',NULL,'$2y$10$YzWKCsKp7Fim/rYaZsKkV.Vdb8b6DKq8NFlrYgkgsT8HAbjgwcYzy',NULL,NULL,NULL,1,1,'2022-07-23 18:04:24','2022-07-23 18:04:24'),
(11,'Test Contact 1','ajayb.225316@gmail.com','5875852220','$2y$10$fa.hPBqZPAxaDeQ7uRsdLuTpfpeYCakMjXmrA9HK9cqAcZXaF8Eam','66d45b0d9fbde4e0ff97ec4d96a5ad9b8846b39c',NULL,NULL,1,1,'2022-08-01 01:22:25','2022-08-01 01:31:36'),
(12,'Test Contact 1','ajayb225316@gmail.com','123456789','$2y$10$SoFvP8CNqyied0rvUzsXKOrJWzj65yXqzXvmM5X7vf6y4N6I2DGsW','66d45b0d9fbde4e0ff97ec4d96a5ad9b8846b39c',NULL,NULL,1,1,'2022-08-01 01:28:44','2022-08-01 01:28:44'),
(13,'Amar','amar@sanmisha.com','9820592630','$2y$10$gk8JfzEHLIJPqLClCiFCrOKbWzZtYQlHw0NdvTjgjAJIlI96sS5Yq','66d45b0d9fbde4e0ff97ec4d96a5ad9b8846b39c',NULL,NULL,1,1,'2022-08-20 11:15:07','2022-08-20 11:15:52'),
(17,'Test Client 1','testclient464@gmail.com','9876543210','$2y$10$MMuzCxJKAQl9T9O0TTleuuLb627Z4/ibCEpdonMBfYxwdimOmirQW','513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,1,1,'2022-09-19 06:09:55','2023-02-05 07:15:00'),
(18,'Test Client','testclient654@gmail.com',NULL,NULL,NULL,NULL,NULL,1,1,'2022-11-12 16:21:03','2022-11-12 16:21:03'),
(19,'Test Client 512','testclient512@gmail.com','5555555555',NULL,'513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,0,1,'2022-11-12 16:34:40','2022-11-12 16:34:40'),
(20,'Frank Ramoz','ramozfrank159@gmail.com','1111111111','$2y$10$s/rq76d5I9NxcmpQE1CKf.F4k71lYpG8Muf588RN8LGvxih0FI0/.','513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,1,1,'2022-11-20 16:27:55','2022-11-20 16:27:55'),
(21,'test client','clienttest808@gmail.com','8888888888',NULL,'513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,0,1,'2022-11-28 04:35:25','2022-11-28 04:35:25'),
(22,'test client','ctest9150@gmail.com','6666666666',NULL,'513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,0,1,'2022-11-30 05:19:18','2022-11-30 05:19:19'),
(23,'test client 105','clienttest624@gmail.com','9656965695',NULL,'513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,0,1,'2022-11-30 05:31:13','2022-11-30 05:31:13'),
(24,'test client 705','clientdemo41@gmail.com','4564564565','$2y$10$YzWKCsKp7Fim/rYaZsKkV.Vdb8b6DKq8NFlrYgkgsT8HAbjgwcYzy','513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,1,1,'2022-11-30 05:42:46','2022-11-30 05:42:46'),
(25,'test','democlient89@gmail.com','56987456985','$2y$10$YzWKCsKp7Fim/rYaZsKkV.Vdb8b6DKq8NFlrYgkgsT8HAbjgwcYzy','513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,1,1,'2022-11-30 05:48:59','2022-11-30 05:48:59'),
(26,'vinod','vs10301030@gmail.com','8976931431','$2y$10$eJJSvv7QrR.rgnP04IIboe/LL60Gnd8LtLPCxWK86Eh3H76TSoFie','513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,1,0,'2022-11-30 11:56:30','2023-04-13 11:24:44'),
(27,'Sanjeev Divekar','sanjeevdivekar@hotmail.com','9820427417','$2y$10$n4K7p2yHGeG/DcBTydW4rOT3giPm1fIPMujKz8DL9Fw.y.J.xlsaG','513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,1,1,'2022-12-22 06:13:55','2022-12-22 06:20:23'),
(28,'Sanjeev Divekar','visitor@listly.com','1234567885','$2y$10$2/gT6JAM0zqWIxz0QYaSMOgXLw6ll5SXhB4nnK.wsYitsFGOy7YaK','513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,1,1,'2023-01-06 06:03:27','2023-01-06 06:03:28'),
(30,'Test Client','tclient909@gmail.com','25252525252','$2y$10$QdEc9O//lf7Di9UXovu4mOsyMJh/BpNPoZW44fS6tehDA/5eSXDfq','513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,1,1,'2023-01-24 04:53:57','2023-01-24 05:04:11'),
(31,'Test02042023','ab5736451@gmail.com','151515151515','$2y$10$PjJYFKz4CurOPplCcb0l6u5kf59M9DX4zkgmCRDZvvPSSeq.tPybq','513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,1,1,'2023-02-05 06:14:34','2023-02-08 05:09:12'),
(32,'Sa','sa@gmail.com','+115888666666777888777777','$2y$10$6VhQusSkdQ8g/Ce84jirvO5CX72PMXw8x2pjboil03nT1ZEsESm9y','513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,1,1,'2023-02-09 18:00:17','2023-02-09 18:00:17'),
(33,'Test Client 0211','tu6877922@gmail.com','7787878787878','$2y$10$mqZw1vp9bsYlmzHL9oCtYunOrbUNgTaLD5fyqal.8azqpxj08PO2m','513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,1,1,'2023-02-11 20:21:11','2023-02-11 20:22:08'),
(34,'Test Client 204','tcl805671@gmail.com','1233333333','$2y$10$RNi5AYbH3/ocEdp2pdoyju4az2c5loeLHq48O7DoOeGeHfmeiVCIm','513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,1,1,'2023-02-21 03:49:14','2023-02-21 03:49:47'),
(35,'test client 205','tc223334@gmail.com','2222222222','$2y$10$uN4iJa5G1168TI0Gm8Dhi.o8ba.adkrR0tB2zhkTlqHK.QSzrVL9W','513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,1,1,'2023-02-21 03:54:53','2023-02-21 03:57:28'),
(36,'test client 207','ctest2786@gmail.com','5858585858','$2y$10$b84F5h4HWkWyIZcYR.9hCu6.j0FQAD/CgndsSmpEpWdYwEPGm4bFm','513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,1,1,'2023-02-21 04:12:50','2023-02-21 04:13:28'),
(37,'test client 601','testclient623@gmail.com','5959595959','$2y$10$VPb8twlX5xnPy2OmfrpeCe41PFZkgYuC3Gsg1I5Lqix/XDpQTDIk2','513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,1,1,'2023-02-22 04:59:00','2023-02-22 04:59:46'),
(38,'Test Name March 10','testn9916@gmail.com','1010101010','$2y$10$loaPAwMFY2Fol5eC4582teDfu.acnGyFYZa41unxicgNVn9qDuLX6','513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,1,1,'2023-03-11 05:31:33','2023-03-11 05:31:55'),
(43,'vinod','vs10201020@gmail.com','2235699856','$2y$10$YzWKCsKp7Fim/rYaZsKkV.Vdb8b6DKq8NFlrYgkgsT8HAbjgwcYzy','513458813ef66f4c757d40cbfc99e7be5c2c2abf',NULL,NULL,1,1,'2023-04-13 07:14:06','2023-04-13 11:24:39');

/*Table structure for table `vendors` */

DROP TABLE IF EXISTS `vendors`;

CREATE TABLE `vendors` (
  `id` int(10) NOT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `referred_by` varchar(255) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `vendors` */

insert  into `vendors`(`id`,`vendor`,`referred_by`,`modified_by`,`modified`,`created_by`,`created`) values 
(2,'Kamlesh Gavali',NULL,NULL,'2021-06-04 15:28:00',NULL,'2021-06-04 15:28:00'),
(3,'Sanjeev Divekar',NULL,NULL,'2021-06-09 11:00:48',NULL,'2021-06-09 11:00:48'),
(4,'anand',NULL,1,'2022-04-02 10:56:51',1,'2022-04-02 10:56:51'),
(5,'anand bhadekar',NULL,NULL,'2022-04-14 09:43:37',NULL,'2022-04-14 09:43:37'),
(9,'Andy Bob',NULL,NULL,'2022-07-11 18:05:35',NULL,'2022-07-11 18:05:35'),
(17,'Test Client 1',NULL,NULL,'2022-09-19 06:09:55',NULL,'2022-09-19 06:09:55'),
(19,'Test Client 512',NULL,NULL,'2022-11-12 16:34:40',NULL,'2022-11-12 16:34:40'),
(21,'test client',NULL,NULL,'2022-11-28 04:35:25',NULL,'2022-11-28 04:35:25'),
(22,'test client',NULL,NULL,'2022-11-30 05:19:18',NULL,'2022-11-30 05:19:18'),
(23,'test client 105',NULL,NULL,'2022-11-30 05:31:13',NULL,'2022-11-30 05:31:13'),
(24,'test client 705',NULL,NULL,'2022-11-30 05:42:46',NULL,'2022-11-30 05:42:46'),
(25,'test',NULL,NULL,'2022-11-30 05:48:59',NULL,'2022-11-30 05:48:59'),
(26,'prathmesh',NULL,NULL,'2022-11-30 11:56:30',NULL,'2022-11-30 11:56:30'),
(27,'Sanjeev Divekar',NULL,NULL,'2022-12-22 06:13:55',NULL,'2022-12-22 06:13:55'),
(31,'Test02042023',NULL,NULL,'2023-02-05 06:14:34',NULL,'2023-02-05 06:14:34'),
(34,'Test Client 204',NULL,NULL,'2023-02-21 03:49:14',NULL,'2023-02-21 03:49:14'),
(35,'test client 205',NULL,NULL,'2023-02-21 03:54:53',NULL,'2023-02-21 03:54:53');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
