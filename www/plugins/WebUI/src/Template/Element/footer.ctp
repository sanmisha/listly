<footer class="plus_border">
		<div class="container margin_60_35">
			<div class="row">
				<div class="col-lg-3 col-md-6 col-sm-6">
					<h3 data-target="#collapse_ft_1">Quick Links</h3>
					<div class="collapse dont-collapse-sm" id="collapse_ft_1">
						<ul class="links">
							<li><a href="/about-us">About us</a></li>
							<li><a href="/terms-and-conditions">Terms And Conditions</a></li>
							<li><a href="/privacy-policy"> Privacy Policies </a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6">
				<h3 data-target="#collapse_ft_2">Categories</h3>
					<div class="collapse dont-collapse-sm" id="collapse_ft_2">
					<ul class="links">

						<?php foreach($this->Ctgy->getFCategory() as $i => $category){ ?>

						<li><a href="/listings/search/<?=$category->slug ?>"><?php echo $category->category; ?></a></li>


						<?php } ?>

</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6">
				<h3 data-target="#collapse_ft_3">Contacts</h3>
					<div class="collapse dont-collapse-sm" id="collapse_ft_3">
						<ul class="contacts">
							<li><i class="ti-headphone-alt"></i><a href="tel:+91 99234 42344">+91 99234 42344</a></li>
							<li><i class="ti-email"></i><a href="mailto:abc@abc.com">abc@abc.com</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6">
					<h3 data-target="#collapse_ft_4">Follow Us</h3>
					<div class="collapse dont-collapse-sm" id="collapse_ft_4">
					<div class="follow_us">
							<ul>
								<li><a href="#0"><i class="ti-facebook"></i></a></li>
								<li><a href="#0"><i class="ti-twitter-alt"></i></a></li>
								<li><a href="#0"><i class="ti-pinterest"></i></a></li>
								<li><a href="#0"><i class="ti-instagram"></i></a></li>
							</ul>
						</div>
				</div>
				</div>
			</div>
			<!-- /row-->
			<hr>
			<div class="row">
				<div class="col-lg-6">
					<!--
					<ul id="footer-selector">
						<li>
							<div class="styled-select" id="lang-selector">
								<select>
									<option value="English" selected>English</option>
									<option value="French">French</option>
									<option value="Spanish">Spanish</option>
									<option value="Russian">Russian</option>
								</select>
							</div>
						</li>
						<li>
							<div class="styled-select" id="currency-selector">
								<select>
									<option value="US Dollars" selected>US Dollars</option>
									<option value="Euro">Euro</option>
								</select>
							</div>
						</li>
						<li><?php //echo $this->Html->image('cards_all.svg', ['alt' => '']);?></li>
					</ul>
					-->
				</div>
				<!--
				<div class="col-lg-6">
					<ul id="additional_links">
						<li><a href="#0">Terms and conditions</a></li>
						<li><a href="#0">Privacy</a></li>
					</ul>
				</div>
				-->
			</div>
		</div>
	</footer>
	<!--/footer-->
	</div>
	<!-- page -->

	<div id="toTop"></div><!-- Back to top button -->


	<!-- COMMON SCRIPTS -->
	<?php
		echo $this->Html->script([
			'functions', 
			'popper.min',
			'bootstrap.min',
			'/assets/validate',
			'/lib/owlcarousel/owl.carousel.min',
			'/lib/waypoints/waypoints.min',
			'/lib/wow/wow.min',
            '/lib/easing/easing.min',
            'rater.min',
			'moment.min',			
			'/vendor/typeahead/dist/jquery.typeahead.min.js',
			'/vendor/typeahead/dist/jquery.typeahead.js',		

		]);
	?>
	
<!-- Messenger Chat plugin Code -->
<div id="fb-root"></div>

<!-- Your Chat plugin code -->
<div id="fb-customer-chat" class="fb-customerchat">
</div>

<script>
  var chatbox = document.getElementById('fb-customer-chat');
  chatbox.setAttribute("page_id", "106802985484498");
  chatbox.setAttribute("attribution", "biz_inbox");
</script>

<!-- Your SDK code -->
<script>
  window.fbAsyncInit = function() {
    FB.init({
      xfbml            : true,
      version          : 'v14.0'
    });
  };

  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>

<!-- category list -->
<script>
	$(document).ready(function(){ 
		var id = $("#category-id").val();

		$.typeahead({
            input: '.listing-type',
            minLength: 0,
            maxItem: 15,
            order: "asc",
            hint: true,
			highlight: true,
            accent: true,
            searchOnFocus: true,
            source: {
                data : [
					<?php   
						foreach($this->List->getListings() as $listing){ ?>
						'<?= $listing ?>',
					<?php } ?>
				]
            },
        });

		<?php if(!empty($sub_category_id)){ ?>
		$.getJSON('/Categories/getCategory/' + id, function(data){
			console.log(data)
			items = data.subCategories;	
			$.each(items, function(key, value) {	
				if(key == <?= $sub_category_id ?>)	{
					$("#sub-category-id")
					.append($("<option></option>")
					.attr("value",key)
					.text(value ));
				}		
				
			});		
			
			$.each(items, function(key, value) {	
				if(value.id != <?= $sub_category_id ?>)	{
					$("#sub-category-id")
					.append($("<option></option>")
					.attr("value",key)
					.text(value));
				}
			});
			
			

		});

		<?php } ?>
		
		//do work
	});
	function categoryChange(){
		var id = $("#category-id").val();		
		$("#sub-category-id").empty();
		$.getJSON('/Categories/getCategory/' + id, function(data){
			console.log(data)
			items = data.subCategories;			
			$("#sub-category-id")
				.append($("<option></option>")
				.attr("value",'')
				.text("- - -"));
			$.each(items, function(key, value) {
				console.log(key)
				$("#sub-category-id")
					.append($("<option></option>")
					.attr("value",key)
					.text(value));
			});

		});

	}

</script>
</body>
</html>
