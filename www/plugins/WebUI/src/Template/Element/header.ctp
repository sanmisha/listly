<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><?php if(isset($title)) { echo $title; } else { echo "Listly"; } ?></title>
    <meta name="description" content="" />
    <meta name="keywords" content="<?php if(isset($keywords)) { echo $keywords; } ?>" />
    <meta name="author" content="Sanmisha Technologies Pvt. Ltd." />

    <meta property="og:title" content="<?php if(isset($title)) { echo $title; } ?>" />
    <meta property="og:description" content="">
    <meta property="og:url" content="<?= Cake\Routing\Router::url(null, true) ?>" />
    <meta property="og:type" content="Website" />
    <!-- baloo 2 font-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Baloo+2&display=swap" rel="stylesheet">
    <!-- Favicons-->
	<?php
		/*echo $this->Html->meta('favicon.ico', 'img/favicon.ico', ['type' => 'icon', 'rel'=>'shortcut icon']);
		echo $this->Html->meta('favicon.ico', 'img/apple-touch-icon-57x57-precomposed.png', ['type' => 'icon', 'rel'=>'apple-touch-icon']);
		echo $this->Html->meta('favicon.ico', 'img/apple-touch-icon-57x57-precomposed.png', ['type' => 'icon', 'rel'=>'apple-touch-icon','size'=>'72x72']);
		echo $this->Html->meta('favicon.ico', 'img/apple-touch-icon-114x114-precomposed.png', ['type' => 'icon', 'rel'=>'apple-touch-icon','size'=>'114x114']);
		echo $this->Html->meta('favicon.ico', 'img/apple-touch-icon-144x144-precomposed.png', ['type' => 'icon', 'rel'=>'apple-touch-icon','size'=>'144x144']);*/
	?>

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- BASE CSS -->
	<?php
		echo $this->Html->css([
			'bootstrap.min',
            'style',
            'vendors',
            'custom',
            'tables',
            '/vendor/summernote/summernote',
            '/vendor/typeahead/dist/jquery.typeahead.min.css',
			'/vendor/typeahead/dist/jquery.typeahead.css',
		]);
		echo $this->fetch('css');

		echo $this->Html->script([
			'common_scripts',
            '/vendor/summernote/summernote.min.js',

		]);
        echo $this->fetch('script');
	?>
    <script>
        $(document).ready(function(){
            if($('.summernote').length) {
                $('.summernote').summernote({
                    toolbar: [
                        // [groupName, [list of button]]
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        // ['font', ['fontname', 'strikethrough', 'superscript', 'subscript']],
                        // ['fontsize', ['fontsize']],
                        // ['color', ['color']],
                        ['para', ['ul', 'ol']],
                        // ['insert', ['picture', 'link', 'video', 'table', 'hr']],
                        //['misc', ['fullscreen', 'codeview', 'undo', 'redo', 'add-text-tags']],
                        // ['misc', ['fullscreen', 'codeview', 'undo', 'redo']],
                    ],
                     popover: {
                        image: [],
                        link: [],
                        air: []
                    },
                    fontNames: ['Verdana', 'Arial', 'Courier New'],
                    fontNamesIgnoreCheck: ['Verdana', 'Arial', 'Courier New'],
                    fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48' , '64', '82', '150']
                });
            }
        });
    </script>
    <style>
      
        .typeahead__list .typeahead__item {
            border: none;
        }
    </style>
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=60921f7b9c605400117b16c9&product=sop' async='async'></script>
</head>

<body>

